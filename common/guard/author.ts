import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class AuthorGuard implements CanActivate {
    constructor() { }
    canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest();
        if (request.headers.user.role == "admin")
            return true
        throw new UnauthorizedException("Unauthorized, only admin has permission");
    }
}