import { HttpException, HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as firebase from 'firebase-admin';
import { DecodedIdToken } from 'firebase-admin/lib/auth/token-verifier';
import { UserRepository } from 'src/user/user.repository';
@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(
        private userRepo: UserRepository
    ) {
    }
    async use(req: Request, res: Response, next: NextFunction) {
        if (req.route.path === '/user/signUp') {
            return next();
        }
        if (!req.headers.authorization) {
            throw new HttpException("Unauthen", HttpStatus.BAD_REQUEST)
        }
        const token = req.headers.authorization.split(" ")[1]
        const firebaseUser: DecodedIdToken | any = await firebase
            .auth()
            .verifyIdToken(token)
            .catch(() => {
                return null
            })
        if (!firebaseUser) { 
            throw new HttpException("Invalid firebase idToken", HttpStatus.BAD_REQUEST)
        }
        req.body.firebaseUser = firebaseUser
        next();

    }
}
