export class PaginateBaseDto<T> {

  public docs: T[];

  public totalPage: number;

  public size: number;

  public count: number;

  public page: number;

  public totalCount: number;
}