import { HttpException, HttpStatus } from "@nestjs/common";
import { isRFC3339 } from "class-validator";
import { PaginateBaseDto } from "common/base/dto/paginate-base.dto";

export abstract class AbstractMongodbRepository<T>{
    constructor(private dataSource) {
    }
    async insert(input) {
        try {
            const rs = await new this.dataSource(input).save()
            return rs
        }
        catch (err) {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
    async update(id: string, data: any): Promise<T> {
        try {
            const rs = await this.dataSource.findOneAndUpdate({ _id: id }, data, { returnDocument: "after" })
            return rs
        }
        catch (err) {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
    async findById(id: string) {
        try {
            return await this.dataSource.findById(id)
        }
        catch (err) {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
    async find(option?: any) {
        try {
            const page = option?.pagination?.page || 1
            const size = option?.pagination?.size || 100
            const where = option?.where || {}
            const orderBy = option?.orderBy || { createdAt: -1 }
            const [list, count] = await Promise.all([
                await this.dataSource.find(where).limit(size).skip(Number(page - 1) * size).sort(orderBy),
                await this.dataSource.count(where)
            ])
            return {
                docs: list,
                size: size,
                page: page,
                count: list.length,
                totalCount: count,
                totalPage: Math.ceil(count / size)
            }
        }
        catch (err) {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
    async findOne(option) {
        try {
            return await this.dataSource.findOne(option)
        }
        catch (err) {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
    async delete(id) {
        try {
            const rs = await this.dataSource.deleteOne({ _id: id })
            if (rs.deletedCount > 0) return true
            return false
        }
        catch (err) {
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}  