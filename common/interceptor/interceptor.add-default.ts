import { CACHE_MANAGER, CallHandler, ExecutionContext, Inject, Injectable, NestInterceptor } from '@nestjs/common';
import { map, Observable, tap } from 'rxjs';
import { LogService } from 'src/log/log.service';
let ttl = Number(process.env.REDIS_DEFAULT_TTL) || 60
@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(
    private logService: LogService
  ) { }
  async intercept(context: ExecutionContext, next: CallHandler): Promise<any> {
    // eslint-disable-next-line prefer-const
    let { method, body, originalUrl, params, query, headers } = context.switchToHttp().getRequest();
    let log = { id: "" }
    if (method == "POST" || method == "PUT" || method == "DELETE") {
      log = await this.logService.create({ method, body, path: originalUrl, param: params, query, header: headers })
    }
    return next.handle().pipe(map(async (value) => {
      if (method == "POST" || method == "PUT" || method == "DELETE") {
        this.logService.update(log.id, { output: value })
      }
      return value
    }));
  }
}
