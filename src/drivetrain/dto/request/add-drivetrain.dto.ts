import { IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddDriveTrainDto {
    @IsNotEmpty()
    @IsString()
    name: string

    @IsOptional()
    @IsString()
    description?: string
}
