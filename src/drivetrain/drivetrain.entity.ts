import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type DriveTrainDocument = DriveTrain & Document;

@Schema({ timestamps: true })
export class DriveTrain {
    @Prop()
    name: string

    @Prop()
    description: string

}
export const DriveTrainSchema = SchemaFactory.createForClass(DriveTrain);