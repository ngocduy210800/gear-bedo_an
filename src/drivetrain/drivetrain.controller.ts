import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { DriveTrainService } from "./drivetrain.service";
import { AddDriveTrainDto } from "./dto/request/add-drivetrain.dto";
import { QueryDriveTrainDto } from "./dto/request/query-drivetrain.dto";
import { UpdateDriveTrainDto } from "./dto/request/update-drivetrain.dto";


@Controller("/drivetrain")
@ApiTags("Drive Train")
export class DriveTrainController {
    constructor(private driveTrainService: DriveTrainService) { }
    @Post()
    create(@Body() body: AddDriveTrainDto) {
        return this.driveTrainService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateDriveTrainDto) {
        return this.driveTrainService.update(id, body)
    }
    @Get()
    get(@Query() queryDriveTrainDto: QueryDriveTrainDto) {
        return this.driveTrainService.get(queryDriveTrainDto)
    }
    @Get("/:id")
    getById(@Param("id") id:string) {
        return this.driveTrainService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id:string) {
        return this.driveTrainService.delete(id)
    }
}