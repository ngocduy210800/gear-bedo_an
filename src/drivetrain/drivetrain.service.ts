import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { DriveTrainRepository } from "./drivetrain.repository";
import { AddDriveTrainDto } from "./dto/request/add-drivetrain.dto";
import { QueryDriveTrainDto } from "./dto/request/query-drivetrain.dto";
import { UpdateDriveTrainDto } from "./dto/request/update-drivetrain.dto";
@Injectable()
export class DriveTrainService {
    constructor(
        private driveTrainRepo: DriveTrainRepository
    ) { }
    async create(input: AddDriveTrainDto) {
        const { name } = input
        const existingName = await this.driveTrainRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.driveTrainRepo.insert(input)
    }
    async update(id, body: UpdateDriveTrainDto) {
        const { name } = body
        const existingDriveTrain = await this.driveTrainRepo.findById(id)
        if (!existingDriveTrain) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if(name){
            const existingName = await this.driveTrainRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.driveTrainRepo.update(id, body)
    }
    async get(query: QueryDriveTrainDto) {
        const option = await this.driveTrainRepo.mapOptionsQuery(query)
        return await this.driveTrainRepo.find(option)
    }
    async getById(id) {
        const driveTrain = await this.driveTrainRepo.findById(id)
        if (!driveTrain) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return driveTrain
    }
    async delete(id) {
        const driveTrain = await this.driveTrainRepo.findById(id)
        if (!driveTrain) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.driveTrainRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}