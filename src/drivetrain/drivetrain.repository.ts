import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { DriveTrain, DriveTrainDocument } from "./drivetrain.entity";
import { QueryDriveTrainDto } from "./dto/request/query-drivetrain.dto";

@Injectable()
export class DriveTrainRepository extends AbstractMongodbRepository<DriveTrain> {
    constructor(
        @InjectModel(DriveTrain.name) private driveTrainModel: Model<DriveTrainDocument>
    ) {
        super(driveTrainModel)
    }
    async mapOptionsQuery(queryDriveTrainDto: QueryDriveTrainDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, name } = queryDriveTrainDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        return options;
    }

}