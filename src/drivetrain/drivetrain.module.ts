import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { DriveTrainController } from "./drivetrain.controller";
import { DriveTrain, DriveTrainSchema } from "./drivetrain.entity";
import { DriveTrainRepository } from "./drivetrain.repository";
import { DriveTrainService } from "./drivetrain.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: DriveTrain.name, schema: DriveTrainSchema }])],
    controllers: [DriveTrainController],
    providers: [DriveTrainRepository, DriveTrainService],
    exports: [DriveTrainRepository]
})
export class DriveTrainModule { }