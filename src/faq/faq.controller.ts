import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryFAQDto } from "./dto/request/query-faq.dto";
import { UpdateFAQDto } from "./dto/request/update-faq.dto";
import { AddFAQDto } from "./dto/request/add-faq.dto";
import { FAQService } from "./faq.service";
import { ApiTags } from "@nestjs/swagger";
@Controller("/faq")
@ApiTags("faq")
export class FAQController {
    constructor(private _service: FAQService) { }
    @Post()
    create(@Body() body: AddFAQDto) {
        return this._service.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateFAQDto) {
        return this._service.update(id, body)
    }
    @Get()
    get(@Query() queryMakeDto: QueryFAQDto) {
        return this._service.get(queryMakeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this._service.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this._service.delete(id)
    }
}