import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { FAQRepository } from "./faq.repository";
import { AddFAQDto } from "./dto/request/add-faq.dto";
import { QueryFAQDto } from "./dto/request/query-faq.dto";
import { UpdateFAQDto } from "./dto/request/update-faq.dto";


@Injectable()
export class FAQService {
    constructor(
        private faqRepo: FAQRepository,
    ) { }
    async create(input: AddFAQDto) {
        return await this.faqRepo.insert(input)
    }
    async update(id, body: UpdateFAQDto) {
        const existingFAQ = await this.faqRepo.findById(id)
        if (!existingFAQ) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        return await this.faqRepo.update(id, body)
    }
    async get(query: QueryFAQDto) {
        const option = await this.faqRepo.mapOptionsQuery(query)
        const faqFind= await this.faqRepo.find(option)
        return faqFind
    }
    async getById(id) {
        const faq = await this.faqRepo.findById(id)
        if (!faq) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return faq
    }
    async delete(id) {
        const faq = await this.faqRepo.findById(id)
        if (!faq) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.faqRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}