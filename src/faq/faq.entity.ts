import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type FAQDocument = FAQ & Document;

@Schema({ timestamps: true })
export class FAQ {
    @Prop()
    question: string

    @Prop()
    answer: string

    @Prop()
    note: string

    @Prop()
    categoryId: string
}
export const FAQSchema = SchemaFactory.createForClass(FAQ);