import { Module } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { FAQCategoryModule } from "src/faq_category/faqcategory.module";
import { FAQController } from "./faq.controller";
import { FAQ, FAQSchema } from "./faq.entity";
import { FAQRepository } from "./faq.repository";
import { FAQService } from "./faq.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: FAQ.name, schema: FAQSchema }]),
    ],
    controllers: [FAQController],
    providers: [FAQRepository, FAQService],
    exports: [FAQRepository]
})
export class FAQModule { }