import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddFAQDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    question: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    answer: string


    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    note: string


    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    categoryId: string

}
