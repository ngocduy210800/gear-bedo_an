import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class UpdateFAQDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    question: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    answer: string


    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    note: string


    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    categoryId: string

}
