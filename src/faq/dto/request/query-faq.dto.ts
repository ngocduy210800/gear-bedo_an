import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsEnum, isJSON, IsNotEmpty, IsOptional, IsString, } from "class-validator";
import * as moment from 'moment'

export class QueryFAQDto {
    @IsOptional()
    @ApiProperty({ required: false })
    public id?: string;

    @IsOptional()
    @ApiProperty({ required: false, default: 1 })
    public page?: Number;

    @IsOptional()
    @ApiProperty({ required: false, default: 100 })
    public size?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    @ApiProperty({ required: false, type: "object", description: "de sort", example: "orderBy:{ name: 1 }" })
    public orderBy?: object;

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    question?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    answer?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    categoryId?: string
    
    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, description: "co the query tu ngay den ngay", example: "createdAt={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public createdAt?: object;
}
