import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { FAQ, FAQDocument } from "./faq.entity";
import { QueryFAQDto } from "./dto/request/query-faq.dto";

@Injectable()
export class FAQRepository extends AbstractMongodbRepository<FAQ> {
    constructor(
        @InjectModel(FAQ.name) private FAQCategoryModel: Model<FAQDocument>
    ) {
        super(FAQCategoryModel)
    }
    async mapOptionsQuery(queryMakeDto: QueryFAQDto) {
        const { page, size, id, orderBy =  { createdAt: -1 }, question, answer, categoryId, createdAt } = queryMakeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (question) { options.where = { ...options.where, question } }
        if (answer) { options.where = { ...options.where, name: { $regex: answer, $options: 'i' } } }
        if (categoryId) { options.where = { ...options.where, categoryId } }
        if (createdAt) { options.where = { ...options.where, createdAt } }
        return options;
    }

}