import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { GarageService } from "./garage.service";
import { QueryGarageDto } from "./dto/request/query-garage.dto";
import { UpdateGarageDto } from "./dto/request/update-garage.dto";
import { AddGarageDto } from "./dto/request/add-garage.dto";
import { ApiSecurity, ApiTags } from "@nestjs/swagger";
@Controller("/garage")
@ApiTags("Garage")
export class GarageController {
    constructor(private garageService: GarageService) { }
    @Post()
    create(@Body() body: AddGarageDto) {
        return this.garageService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateGarageDto) {
        return this.garageService.update(id, body)
    }
    @Get()
    get(@Query() queryGarageDto: QueryGarageDto) {
        return this.garageService.get(queryGarageDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.garageService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.garageService.delete(id)
    }
}