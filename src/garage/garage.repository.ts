import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Garage, GarageDocument } from "./garage.entity";
import { QueryGarageDto } from "./dto/request/query-garage.dto";

@Injectable()
export class GarageRepository extends AbstractMongodbRepository<Garage> {
    constructor(
        @InjectModel(Garage.name) private garageModel: Model<GarageDocument>
    ) {
        super(garageModel)
    }
    async mapOptionsQuery(queryFuelTypeDto: QueryGarageDto) {
        const { page, size, id, createdAt, orderBy =  { createdAt: -1 }, name, address, description,hotline } = queryFuelTypeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (address) { options.where = { ...options.where, address:{ $regex: address, $options: 'i' } } }
        if (description) { options.where = { ...options.where, description } }
        if (hotline) { options.where = { ...options.where, hotline } }
        return options;
    }
}