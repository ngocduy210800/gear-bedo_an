import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type GarageDocument = Garage & Document;

@Schema({ timestamps: true })
export class Garage {
    @Prop()
    name: string

    @Prop()
    address: string
    
    @Prop()
    hotline: string

    @Prop()
    description: string

}
export const GarageSchema = SchemaFactory.createForClass(Garage);