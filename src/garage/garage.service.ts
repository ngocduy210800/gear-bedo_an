import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { GarageRepository } from "./garage.repository";
import { AddGarageDto } from "./dto/request/add-garage.dto";
import { QueryGarageDto } from "./dto/request/query-garage.dto";
import { UpdateGarageDto } from "./dto/request/update-garage.dto";


@Injectable()
export class GarageService {
    constructor(
        private garageRepo: GarageRepository
    ) { }
    async create(input: AddGarageDto) {
        const { name } = input
        const existingName = await this.garageRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.garageRepo.insert(input)
    }
    async update(id, body: UpdateGarageDto) {
        const { name } = body
        const existingGarage = await this.garageRepo.findById(id)
        if (!existingGarage) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.garageRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.garageRepo.update(id, body)
    }
    async get(query: QueryGarageDto) {
        const option = await this.garageRepo.mapOptionsQuery(query)
        return await this.garageRepo.find(option)
    }
    async getById(id) {
        const garage = await this.garageRepo.findById(id)
        if (!garage) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return garage
    }
    async delete(id) {
        const garage = await this.garageRepo.findById(id)
        if (!garage) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.garageRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}