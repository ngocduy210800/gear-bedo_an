import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsOptional, IsString, } from "class-validator";

export class UpdateGarageDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false})
    name?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false})
    address?: boolean

    @IsOptional()
    @IsString()
    @ApiProperty()
    hotline: boolean

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false})
    description?: string
}
