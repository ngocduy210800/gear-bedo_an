import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddGarageDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    name: string
    
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    address: boolean
    
    @IsOptional()
    @IsString()
    @ApiProperty()
    hotline: boolean

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

}
