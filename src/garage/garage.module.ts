import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { GarageController } from "./garage.controller";
import { Garage, GarageSchema } from "./garage.entity";
import { GarageRepository } from "./garage.repository";
import { GarageService } from "./garage.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: Garage.name, schema: GarageSchema }])],
    controllers: [GarageController],
    providers: [GarageRepository, GarageService],
    exports: [GarageRepository]
})
export class GarageModule { }