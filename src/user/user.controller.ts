import { Body, Controller, Get, HttpException, HttpStatus, Param, Post, Put, Query, UseGuards } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { UserService } from "./user.service";
import { AddUserDto } from "./dto/request/add-user.dto";
import { AddAuthDto } from "./dto/request/add-auth.dto";
import { QueryUserDto } from "./dto/request/query-user.dto";
import { ResponseUserDto } from "./dto/respone/respone-user.dto";
import { UpdateUserDto } from "./dto/request/update-user.dto";
import { ApiBody, ApiHeader, ApiQuery, ApiTags } from "@nestjs/swagger";
import { EditFavouriteCarDto } from "./dto/request/edit-favouritecar.dto";
import { AuthorGuard } from "common/guard/author";

@Controller("/user")
@ApiTags("user")
export class UserController {
    constructor(private userService: UserService) { }
    @Post("/signUp")
    @ApiBody({ type: AddAuthDto, description: "api dki user trên firestore đồng thời đồng bộ phía backend" })
    signUp(@Body() body: AddAuthDto) {
        return  this.userService.signUp(body)
    }
    @Post()
    @ApiBody({ type: AddUserDto, description: "api dki user, sau khi dki bên firebase, gửi lên token và email(hoặc thêm thông tin) để đồng bộ phía backend" })
    create(@Body() body: AddUserDto) {
        return this.userService.create(body)
    }
    @Put("/:id")
    @ApiBody({ type: UpdateUserDto, description: "cap nhat thong tin user" })
    update(@Param("id") id: string, @Body() body: UpdateUserDto) {
        return this.userService.update(id, body)
    }
    @Get()
    //@UseGuards(AuthorGuard)
    get(@Query() queryUserDto: QueryUserDto) {
        return this.userService.get(queryUserDto)
    }
    @Get("/get-user-by-token")
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
      })
    getUserByToken(@Body() body) {
        return this.userService.getUserByToken(body)
    }
    @Get("/:id")
    //@ReponseDto(ResponseUserDto)
    getById(@Param("id") id: string) {
        return this.userService.getById(id)
    }
    @Post("/favouritecar/:iduser")
    @ApiBody({ type: EditFavouriteCarDto, description: "api cập nhật các xe yêu thích của user, gửi lên carsellId " })
    editFavouriteCar(@Param("iduser") id: string, @Body() body: EditFavouriteCarDto) {
        return this.userService.editFavouriteCar(id, body)
    }
}