import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from "./user.entity";
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { QueryUserDto } from "./dto/request/query-user.dto";
@Injectable()
export class UserRepository extends AbstractMongodbRepository<User> {
    constructor(
        @InjectModel(User.name) private userModel: Model<UserDocument>
    ) {
        super(userModel)
    }
    async mapOptionsQuery(queryUserDto: QueryUserDto) {
        const { page, size, id, orderBy = { createdAt: -1 }, firstName, lastName, postalCode, province, city, country, address, dateOfBirth, createdAt, phone, role, favouriteCarId } = queryUserDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (firstName) { options.where = { ...options.where, firstName } }
        if (lastName) { options.where = { ...options.where, lastName } }
        if (postalCode) { options.where = { ...options.where, postalCode } }
        if (province) { options.where = { ...options.where, province } }
        if (city) { options.where = { ...options.where, city } }
        if (country) { options.where = { ...options.where, country } }
        if (address) { options.where = { ...options.where, address } }
        if (dateOfBirth) { options.where = { ...options.where, dateOfBirth } }
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (phone) { options.where = { ...options.where, phone } }
        if (role) { options.where = { ...options.where, role } }
        if (favouriteCarId) { options.where = { ...options.where, favouriteCarId } }
        return options;
    }

}