import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { UserRepository } from "./user.repository";
import * as firebase from 'firebase-admin';
import { DecodedIdToken } from "firebase-admin/lib/auth/token-verifier";
import { AddUserDto } from "./dto/request/add-user.dto";
import { AddAuthDto } from "./dto/request/add-auth.dto";
import { QueryUserDto } from "./dto/request/query-user.dto";
import { UpdateUserDto } from "./dto/request/update-user.dto";
import { CarSellRepository } from "src/carsell/carsell.repository";
import { EditFavouriteCarDto } from "./dto/request/edit-favouritecar.dto";
@Injectable()
export class UserService {
    constructor(
        private userRepo: UserRepository,
        private carSellRepo: CarSellRepository
    ) { }
    async signUp(body: AddAuthDto) {
        const { firstName, lastName, phone, email, password } = body
        const result = await firebase.auth().createUser({
            password: password,
            email: email
        }).then( function(_userRecord) {
            return _userRecord
        }).catch((error) => {
            return error
        })
        if (result.uid) {
            return this.userRepo.insert({ firstName, lastName, phone, email, firId: result.uid })
        } else {
            throw new HttpException(result, HttpStatus.BAD_REQUEST)
        }
    }
    async create(body: AddUserDto) {
        const { firstName, lastName, phone, email, address, city, country, province, postalCode, photoUrl, firebaseUser } = body
        const existingUser = await this.userRepo.findOne({ firId: firebaseUser.uid })
        //check tai khoan ton tai chua
        if (existingUser) return existingUser
        else {
            return this.userRepo.insert({ firstName, lastName, phone, email, address, city, country, province, postalCode, photoUrl, firId: firebaseUser.uid })
        }
    }
    async update(id, body: UpdateUserDto) {
        const existingUser = await this.userRepo.findById(id)
        if (!existingUser) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        return await this.userRepo.update(id, body)
    }
    async get(queryUserDto: QueryUserDto) {
        const option = await this.userRepo.mapOptionsQuery(queryUserDto)
        return await this.userRepo.find(option)
    }
    async getById(id) {
        const user = await this.userRepo.findById(id)
        if (!user) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const favouriteCar = await Promise.all(user.favouriteCarId.map(async id => {
            const carSellItem = await this.carSellRepo.findById(id)
            return carSellItem
        }))
        Object.assign(user._doc, {
            favouriteCar
        })
        return user
    }
    async editFavouriteCar(id: string, body: EditFavouriteCarDto) {
        const { carSellId } = body
        const [existingCarSell, existingUser] = await Promise.all([
            this.carSellRepo.findById(carSellId),
            this.userRepo.findById(id)
        ])
        if (!existingCarSell) throw new HttpException("Not found car", HttpStatus.BAD_REQUEST)
        if (!existingUser) throw new HttpException("Not found user", HttpStatus.BAD_REQUEST)
        if (existingUser.favouriteCarId.includes(carSellId)) {
            existingUser.favouriteCarId = existingUser.favouriteCarId.filter(e => e != carSellId)
        }
        else {
            existingUser.favouriteCarId.push(carSellId)
        }
        const listFavouriteCar = existingUser.favouriteCarId
        return await this.userRepo.update(id, { favouriteCarId: listFavouriteCar })
    }
    async getUserByToken(body) {
        const { firebaseUser } = body
        const existingUser = await this.userRepo.findOne({ firId: firebaseUser.uid })
        //check tai khoan ton tai chua
        if (!existingUser) throw new HttpException("User not found",HttpStatus.BAD_REQUEST)
        return existingUser
    }
}