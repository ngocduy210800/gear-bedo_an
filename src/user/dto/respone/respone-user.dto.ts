import { Exclude, Expose, Transform } from "class-transformer";

@Exclude()
export class ResponseUserDto {
    @Expose()
    id: string;

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose()
    phone: string;

    @Expose()
    email: string;

    @Expose()
    address: string;

    @Expose()
    city: string;

    @Expose()
    country: string;

    @Expose()
    province: string;

    @Expose()
    postalCode: string;

    @Expose()
    photoUrl: string

    @Expose()
    createdAt?: Date

    @Expose()
    favouriteCarId: string[]
}
