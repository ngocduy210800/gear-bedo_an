import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class UpdateUserDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    firstName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    lastName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    address?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    city?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    country?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    phone?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    province?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    postalCode?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    photoUrl?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    note?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required :false})
    dateOfBirth?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false, enum: ["admin", "customer"] })
    role?: string
}
