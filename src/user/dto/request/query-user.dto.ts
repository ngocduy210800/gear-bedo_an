import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsObject, IsJSON, isJSON, IsString } from 'class-validator';
import * as moment from 'moment'
export class QueryUserDto {

    @IsOptional()
    @ApiProperty({ required: false })
    public id?: string;

    @IsOptional()
    @ApiProperty({ required: false, default: 1 })
    public page?: Number;

    @IsOptional()
    @ApiProperty({ required: false, default: 100 })
    public size?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    @ApiProperty({ required: false, type: "object", description: "de sort", example: "orderBy:{ name: 1 }" })
    public orderBy?: object;

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    firstName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    lastName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    address?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    city?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    country?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    province?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    postalCode?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    dateOfBirth?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    phone?: string

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "object", description: "co the query tu ngay den ngay", example: "createdAt={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public createdAt?: object;

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    role?: string

    @IsOptional()
    @ApiProperty({ required: false })
    favouriteCarId: string
}
