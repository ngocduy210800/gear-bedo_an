import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddUserDto {
    @IsNotEmpty()
    firebaseUser: any

    @ApiProperty({ required: false })
    @IsOptional()
    @IsString()
    firstName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    lastName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    phone: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    email: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    address?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    city?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    country?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    province?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    postalCode?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    photoUrl?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    note?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    dateOfBirth?: string

    @IsOptional()
    @IsString()
    @IsEnum(["admin", "customer"])
    @ApiProperty({ required: false, enum: ["admin", "customer"],description:"có 2 role là admin và customer, không truyền lên default là customer" })
    role?: string
}
