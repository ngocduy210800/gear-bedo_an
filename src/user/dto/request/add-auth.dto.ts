import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddAuthDto {
    @ApiProperty({ required: false })
    @IsOptional()
    @IsString()
    firstName?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    lastName?: string

    @IsOptional()
    @ApiProperty({ required: false })
    phone: string | number

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    email: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    password: string
}
