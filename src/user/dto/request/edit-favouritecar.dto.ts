import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, } from "class-validator";

export class EditFavouriteCarDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({ description: "id carsell", required: true })
    carSellId: string
    
}
