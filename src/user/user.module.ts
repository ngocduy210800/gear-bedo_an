import { forwardRef, MiddlewareConsumer, Module, NestModule, RequestMethod } from "@nestjs/common";
import { UserController } from "./user.controller";
import { User, UserSchema } from "./user.entity";
import { MongooseModule } from '@nestjs/mongoose';
import { UserRepository } from "./user.repository";
import { UserService } from "./user.service";
import { CarSellModule } from "src/carsell/carsell.module";
import { AuthMiddleware } from "common/middleware/auth";
@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        forwardRef(() => CarSellModule)
    ],
    controllers: [UserController],
    providers: [UserRepository, UserService],
    exports: [UserRepository]
})
export class UserModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            .exclude({ path: 'user/signUp', method: RequestMethod.GET },
                { path: 'user/signUp', method: RequestMethod.POST },
                { path: 'user', method: RequestMethod.GET },
                { path: 'user/:id', method: RequestMethod.GET },
                { path: 'user/favouritecar/:iduser', method: RequestMethod.POST }
            )
            .forRoutes(UserController)
    }
}