import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
    @Prop()
    firstName: string

    @Prop()
    lastName: string

    @Prop()
    phone: string

    @Prop()
    email: string

    @Prop()
    address: string

    @Prop()
    city: string

    @Prop()
    country: string

    @Prop()
    province: string

    @Prop()
    postalCode: string

    @Prop()
    photoUrl: string

    @Prop()
    note: string

    @Prop()
    dateOfBirth: Date

    @Prop({ type: [String], default: [] })
    favouriteCarId: string[]

    @Prop()
    firId: string//id luu tren firebase

    @Prop({default: "customer" })
    role: string
}
export const UserSchema = SchemaFactory.createForClass(User);