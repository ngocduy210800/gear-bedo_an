import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { BodyTypeModule } from './bodytype/bodytype.module';
import { FuelTypeModule } from './fueltype/fueltype.module';
import { DriveTrainModule } from './drivetrain/drivetrain.module';
import { CylindersModule } from './cylinders/cylinders.module';
import { CarFeatureModule } from './carfeature/carfeature.module';
import { CarSellModule } from './carsell/carsell.module';
import { MakeModule } from './make/make.module';
import { ModelModule } from './model/model.module';
import { TrimModule } from './trim/trim.module';
import { BuyMoreModule } from './buymore/buymore.module';
import { CarBuyModule } from './cars-buy/carbuy.module';
import { FAQCategoryModule } from './faq_category/faqcategory.module';
import { FAQModule } from './faq/faq.module';
import { AuthMiddleware } from 'common/middleware/auth';
import { SearchFilterModule } from './searchfilter/searchfilter.module';
import { CareerModule } from './career/career.module';
import { GarageModule } from './garage/garage.module';
import { LogModule } from './log/log.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from 'common/interceptor/interceptor.add-default';
import { S3Module } from './s3/s3.module';
import { FormRequestModule } from './form-request/form-request.module';
@Module({
  imports: [
    MongooseModule.forRoot(process.env.URL_MONGO_CONNECT),
    UserModule,
    BodyTypeModule,
    FuelTypeModule,
    DriveTrainModule,
    CylindersModule,
    CarFeatureModule,
    CarSellModule,
    MakeModule,
    ModelModule,
    TrimModule,
    BuyMoreModule,
    CarBuyModule,
    FAQCategoryModule,
    FAQModule,
    SearchFilterModule,
    CareerModule,
    GarageModule,
    LogModule,
    S3Module,
    FormRequestModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    }
  ],
})
//export class AppModule {}
export class AppModule { }
