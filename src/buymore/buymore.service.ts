import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { BuyMoreRepository } from "./buymore.repository";
import { AddBuyMoreDto } from "./dto/request/add-buymore.dto";
import { QueryBuyMoreDto } from "./dto/request/query-buymore.dto";
import { UpdateBuyMoreDto } from "./dto/request/update-buymore.dto";


@Injectable()
export class BuyMoreService {
    constructor(
        private buyMoreRepo: BuyMoreRepository
    ) { }
    async create(input: AddBuyMoreDto) {
        const { name } = input
        const existingName = await this.buyMoreRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.buyMoreRepo.insert(input)
    }
    async update(id, body: UpdateBuyMoreDto) {
        const { name } = body
        const existingBuyMore = await this.buyMoreRepo.findById(id)
        if (!existingBuyMore) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.buyMoreRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.buyMoreRepo.update(id, body)
    }
    async get(query: QueryBuyMoreDto) {
        const option = await this.buyMoreRepo.mapOptionsQuery(query)
        return await this.buyMoreRepo.find(option)
    }
    async getById(id) {
        const buyMore = await this.buyMoreRepo.findById(id)
        if (!buyMore) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return buyMore
    }
    async delete(id) {
        const buyMore = await this.buyMoreRepo.findById(id)
        if (!buyMore) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.buyMoreRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}