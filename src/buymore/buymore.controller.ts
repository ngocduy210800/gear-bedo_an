import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { BuyMoreService } from "./buymore.service";
import { AddBuyMoreDto } from "./dto/request/add-buymore.dto";
import { QueryBuyMoreDto } from "./dto/request/query-buymore.dto";
import { UpdateBuyMoreDto } from "./dto/request/update-buymore.dto";


@Controller("/buymore")
@ApiTags("Buy More")
export class BuyMoreController {
    constructor(private buyMoreService: BuyMoreService) { }
    @Post()
    create(@Body() body: AddBuyMoreDto) {
        return this.buyMoreService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateBuyMoreDto) {
        return this.buyMoreService.update(id, body)
    }
    @Get()
    get(@Query() queryBuyMoreDto: QueryBuyMoreDto) {
        return this.buyMoreService.get(queryBuyMoreDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.buyMoreService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.buyMoreService.delete(id)
    }
}