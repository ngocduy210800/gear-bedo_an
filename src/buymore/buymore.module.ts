import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { BuyMoreController } from "./buymore.controller";
import { BuyMore, BuyMoreSchema } from "./buymore.entity";
import { BuyMoreRepository } from "./buymore.repository";
import { BuyMoreService } from "./buymore.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: BuyMore.name, schema: BuyMoreSchema }])],
    controllers: [BuyMoreController],
    providers: [BuyMoreRepository, BuyMoreService],
    exports: [BuyMoreRepository]
})
export class BuyMoreModule { }