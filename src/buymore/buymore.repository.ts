import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { BuyMore, BuyMoreDocument } from "./buymore.entity";
import { QueryBuyMoreDto } from "./dto/request/query-buymore.dto";

@Injectable()
export class BuyMoreRepository extends AbstractMongodbRepository<BuyMore> {
    constructor(
        @InjectModel(BuyMore.name) private buyMoreModel: Model<BuyMoreDocument>
    ) {
        super(buyMoreModel)
    }
    async mapOptionsQuery(queryBodyTypeDto: QueryBuyMoreDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, name, price } = queryBodyTypeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (price) { options.where = { ...options.where, price } }
        return options;
    }

}