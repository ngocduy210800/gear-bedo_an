import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString, } from "class-validator";

export class UpdateBuyMoreDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    price: number

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    note?: string
}
