import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type BuyMoreDocument = BuyMore & Document;

@Schema({ timestamps: true })
export class BuyMore {
    @Prop()
    name: string

    @Prop()
    description: string

    @Prop()
    price: number

    @Prop()
    note: string
}
export const BuyMoreSchema = SchemaFactory.createForClass(BuyMore);