import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type SearchFilterDocument = SearchFilter & Document;

@Schema({ timestamps: true })
export class SearchFilter {
    @Prop()
    name:string

    @Prop()
    userId: string

    @Prop({ type: Object })
    search: object

    @Prop({ type: Object })
    tag: object
}
export const SearchFilterSchema = SchemaFactory.createForClass(SearchFilter);