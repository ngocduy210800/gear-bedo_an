import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { SearchFilter, SearchFilterDocument } from "./searchfilter.entity";
import { QueryearchFilterDto } from "./dto/request/query-searchfilter.dto";

@Injectable()
export class SearchFilterRepository extends AbstractMongodbRepository<SearchFilter> {
    constructor(
        @InjectModel(SearchFilter.name) private searchFilter: Model<SearchFilterDocument>
    ) {
        super(searchFilter)
    }
    async mapOptionsQuery(queryModelDto: QueryearchFilterDto) {
        const { page, size, id, orderBy = { createdAt: -1 }, userId, createdAt, name } = queryModelDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (userId) { options.where = { ...options.where, userId } }
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (name) { options.where = { ...options.where, name } }
        return options;
    }

}