import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { ModelModule } from "src/model/model.module";
import { SearchFilterController } from "./searchfilter.controller";
import { SearchFilter, SearchFilterSchema } from "./searchfilter.entity";
import { SearchFilterRepository } from "./searchfilter.repository";
import { SearchFilterService } from "./searchfilter.service";



@Module({
    imports: [
        MongooseModule.forFeature([{ name: SearchFilter.name, schema: SearchFilterSchema }]),
        ModelModule
    ],
    controllers: [SearchFilterController],
    providers: [SearchFilterRepository, SearchFilterService],
    exports: [SearchFilterRepository]
})
export class SearchFilterModule { }