import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddSearchFilterDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({required: true})
    name: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    userId: string

    @IsNotEmpty()
    @ApiProperty({ required: true })
    search: object

    @IsOptional()
    @ApiProperty({ required: true })
    tag: object

}
