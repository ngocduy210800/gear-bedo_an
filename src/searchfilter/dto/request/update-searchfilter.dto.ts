import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class UpdateSearchFilterDto {
    @IsOptional()
    @IsString()
    @ApiProperty({required: true})
    name: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: true })
    userId: string

    @IsOptional()
    @ApiProperty({ required: true })
    search: object

    @IsOptional()
    @ApiProperty({ required: true })
    tag: object
}
