import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { SearchFilterRepository } from "./searchfilter.repository";
import { AddSearchFilterDto } from "./dto/request/add-searchfilter.dto";
import { QueryearchFilterDto } from "./dto/request/query-searchfilter.dto";
import { UpdateSearchFilterDto } from "./dto/request/update-searchfilter.dto";



@Injectable()
export class SearchFilterService {
    constructor(
        private searchFilterRepo: SearchFilterRepository
    ) { }
    async create(input: AddSearchFilterDto) {
        const { name, userId } = input
        const existingName = await this.searchFilterRepo.findOne({ name, userId })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.searchFilterRepo.insert(input)
    }
    async get(query: QueryearchFilterDto) {
        const option = await this.searchFilterRepo.mapOptionsQuery(query)
        let result = await this.searchFilterRepo.find(option)
        return result
    }
    async update(id, body: UpdateSearchFilterDto) {
        const { name, userId } = body
        const existingSearchFilter = await this.searchFilterRepo.findById(id)
        if (!existingSearchFilter) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.searchFilterRepo.findOne({ name, userId: userId || existingSearchFilter.userId })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.searchFilterRepo.update(id, body)
    }
    async getById(id) {
        const searchFilter = await this.searchFilterRepo.findById(id)
        if (!searchFilter) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return searchFilter
    }
    async delete(id) {
        const searchFilter = await this.searchFilterRepo.findById(id)
        if (!searchFilter) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.searchFilterRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}