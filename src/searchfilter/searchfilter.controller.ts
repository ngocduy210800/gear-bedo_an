import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryearchFilterDto } from "./dto/request/query-searchfilter.dto";
import { AddSearchFilterDto } from "./dto/request/add-searchfilter.dto";
import { ApiTags } from "@nestjs/swagger";
import { SearchFilterService } from "./searchfilter.service";
import { UpdateSearchFilterDto } from "./dto/request/update-searchfilter.dto";
@Controller("/searchfilter")
@ApiTags("Search Filter")
export class SearchFilterController {
    constructor(private searchFilterService: SearchFilterService) { }
    @Post()
    create(@Body() body: AddSearchFilterDto) {
        return this.searchFilterService.create(body)
    }
    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateSearchFilterDto) {
        return this.searchFilterService.update(id, body)
    }
    @Get()
    get(@Query() queryMakeDto: QueryearchFilterDto) {
        return this.searchFilterService.get(queryMakeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.searchFilterService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.searchFilterService.delete(id)
    }
}