import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { GREAR_BOX, STATUS_CAR_SELL } from './constant/constant'
export type CarSellDocument = CarSell & Document;

@Schema({ timestamps: true, collection: "car_sell" })
export class CarSell {

    @Prop()
    nameCar: string

    @Prop()
    yearOfManufacture: number

    @Prop()
    mileage: number

    @Prop({ enum: GREAR_BOX })
    gearBox: string

    @Prop()
    engineSize: number

    @Prop()
    color: string

    @Prop()
    bodyTypeId: string

    @Prop()
    driveTrainId: string

    @Prop()
    cylindersId: string

    @Prop()
    makeId: string

    @Prop()
    modelId: string

    @Prop()
    plate: string

    @Prop()
    vin: string

    @Prop()
    trimId: string

    @Prop()
    fuelTypeId: string

    @Prop()
    doors: number

    @Prop()
    seats: number

    @Prop()
    fuelEconomyCity: number

    @Prop()
    fuelEconomyHighway: number

    @Prop()
    numberOfAccidents: number

    @Prop()
    numberOfKeys: number

    @Prop()
    numberOfOwners: number

    @Prop({ type: "object" })
    images: object

    @Prop({ type: [String] })
    featuresId: string[]

    @Prop()
    userSellId: string

    @Prop({ enum: STATUS_CAR_SELL })
    status: number

    @Prop()
    action: string

    @Prop()
    updateBy: string

    @Prop()
    priceBuy: number

    @Prop()
    offerSell: number

    @Prop()
    taxRate: number

    @Prop({ type: "object" })
    tellUsAboutYourself: object

    @Prop()
    expiresOn: Date

    @Prop({ type: "object" })
    dropOffInfor: object

    @Prop({ type: "object" })
    pickUpInfor: object

    @Prop({ type: "object" })
    checkCarInforReport: object

    @Prop({ type: "object" })
    checkCarInforValuation: object

    @Prop()
    exterior: string

    @Prop()
    interior: string

    @Prop()
    aspiration: string

    @Prop()
    engineDisplacement: String

    @Prop()
    horsepower: string

    @Prop()
    torque: string

    @Prop()
    fuelTankCapacity: string

    @Prop()
    cargoCapacity: string

    @Prop()
    frontLegRoom: string

    @Prop()
    frontHeadRoom: string

    @Prop()
    backLegRoom: string

    @Prop()
    backHeadRoom: string

    @Prop()
    point: number

    @Prop({ type: "object" })
    report: object

    @Prop()
    modifications: boolean

    @Prop()
    personalisedPlate: boolean

    @Prop()
    outstandingFinance: boolean

    @Prop()
    serviceHistory: string

    @Prop()
    condition: string
}
export const CarSellSchema = SchemaFactory.createForClass(CarSell);