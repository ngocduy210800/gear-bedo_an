import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ApiHeader, ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { CarSelleService } from "./carsell.service";
import { AddCarSellDto } from "./dto/add-carsell.dto";
import { OfferPlate } from "./dto/plate-carsell.dto";
import { QueryCarSellDto } from "./dto/query-carsell.dto";
import { UpdateCarSellDto } from "./dto/update-carsell.dto";
@Controller("/carsell")
@ApiTags("Car Sell")
export class CarSellController {
    constructor(private carSellService: CarSelleService) { }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Post()
    create(@Body() body: AddCarSellDto) {
        return this.carSellService.create(body)
    }
    @Get("/:id")
    getByid(@Param("id") id: string) {
        return this.carSellService.getById(id)
    }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.carSellService.delete(id)
    }
    @Get()
    get(@Query() query: QueryCarSellDto) {
        return this.carSellService.get(query)
    }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateCarSellDto) {
        return this.carSellService.update(id, body)
    }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Post("/offer")
    createOffer(@Body() body: OfferPlate) {
        return this.carSellService.createOffer(body)
    }
}