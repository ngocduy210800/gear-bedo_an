import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { throws } from "assert";
import axios from "axios";
import { BodyTypeRepository } from "src/bodytype/bodytype.repository";
import { CarFeatureRepository } from "src/carfeature/carfeature.repository";
import { CylindersRepository } from "src/cylinders/cylinders.repository";
import { DriveTrainRepository } from "src/drivetrain/drivetrain.repository";
import { FuelTypeRepository } from "src/fueltype/fueltype.repository";
import { MakeRepository } from "src/make/make.repository";
import { ModelRepository } from "src/model/model.repository";
import { TrimRepository } from "src/trim/trim.repository";
import { UserRepository } from "src/user/user.repository";
import { CarSellRepository } from "./carsell.repository"
import { AddCarSellDto } from "./dto/add-carsell.dto";
import { UpdateCarSellDto } from "./dto/update-carsell.dto";
import * as moment from "moment";
import { LogRepository } from "src/log/log.repository";
import { IsArray } from "class-validator";
import e from "express";
@Injectable()
export class CarSelleService {
    constructor(
        private carSellRepo: CarSellRepository,
        private bodyTypeRepo: BodyTypeRepository,
        private driveTrainRepo: DriveTrainRepository,
        private carFeatureRepo: CarFeatureRepository,
        private cylindersRepo: CylindersRepository,
        private makeRepo: MakeRepository,
        private modelRepo: ModelRepository,
        private trimRepo: TrimRepository,
        private fuelTypeRepo: FuelTypeRepository,
        private userRepo: UserRepository,
        private logRepo: LogRepository,
    ) { }
    async create(input: AddCarSellDto) {
        const { bodyTypeId, driveTrainId, cylindersId, makeId, modelId, trimId, fuelTypeId, featuresId, userSellId } = input
        const [existingBodyType, existingDriveTrain, existingCylinders, existingMake, existingModel, existingTrim, existingFuelType, existingUser] = await Promise.all([
            this.bodyTypeRepo.findById(bodyTypeId),
            this.driveTrainRepo.findById(driveTrainId),
            this.cylindersRepo.findById(cylindersId),
            this.makeRepo.findById(makeId),
            this.modelRepo.findById(modelId),
            this.trimRepo.findById(trimId),
            this.fuelTypeRepo.findById(fuelTypeId),
            this.userRepo.findById(userSellId)
        ])
        if (makeId) {
            if (!existingMake) throw new HttpException("Not Found Make", HttpStatus.BAD_REQUEST)
        }
        if (modelId) {
            if (!existingModel) throw new HttpException("Not Found Model", HttpStatus.BAD_REQUEST)
        }
        if (trimId) {
            if (!existingTrim) throw new HttpException("Not Found Trim", HttpStatus.BAD_REQUEST)
        }
        if (bodyTypeId) {
            if (!existingBodyType) throw new HttpException("Not Found BodyType", HttpStatus.BAD_REQUEST)
        }
        if (driveTrainId) {
            if (!existingDriveTrain) throw new HttpException("Not Found DriveTrain", HttpStatus.BAD_REQUEST)
        }
        if (cylindersId) {
            if (!existingCylinders) throw new HttpException("Not Found Cylinders", HttpStatus.BAD_REQUEST)
        }
        if (fuelTypeId) {
            if (!existingFuelType) throw new HttpException("Not Found FuelType", HttpStatus.BAD_REQUEST)
        }
        if (featuresId) {
            await Promise.all(featuresId.map(async id => {
                const existingCarFeature = await this.carFeatureRepo.findById(id)
                if (!existingCarFeature) throw new HttpException(`Not found this carFeatureId ${id}`, HttpStatus.BAD_REQUEST)
            }))
        }
        if (userSellId) {
            if (!existingUser) throw new HttpException("Not Found UserSell", HttpStatus.BAD_REQUEST)
        }
        return await this.carSellRepo.insert(input)
    }
    async getById(id) {
        let carSell = await this.carSellRepo.findById(id)
        if (!carSell) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const [bodyType, driveTrain, cylinders, fuelType, userSell, make, model, trim, featuresCar] = await Promise.all([
            this.bodyTypeRepo.findById(carSell.bodyTypeId),
            this.driveTrainRepo.findById(carSell.driveTrainId),
            this.cylindersRepo.findById(carSell.cylindersId),
            this.fuelTypeRepo.findById(carSell.fuelTypeId),
            this.userRepo.findById(carSell.userSellId),
            this.makeRepo.findById(carSell.makeId),
            this.modelRepo.findById(carSell.modelId),
            this.trimRepo.findById(carSell.trimId),
            Promise.all(carSell.featuresId.map(async id => {
                const featuresItem = await this.carFeatureRepo.findById(id)
                return featuresItem
            }))
        ])
        Object.assign(carSell._doc, {
            bodyType,
            driveTrain,
            cylinders,
            fuelType,
            userSell,
            make,
            model,
            trim,
            featuresCar
        })
        return carSell
    }
    async delete(id) {
        const carSell = await this.carSellRepo.findById(id)
        if (!carSell) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.carSellRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
    async get(queryCarSellDto) {
        const option = await this.carSellRepo.mapOptionsQuery(queryCarSellDto)
        let filter = []
        if (option.where["doors"]) {
            if (option.where["doors"] == "2-3") {
                option.where["doors"] = { $gte: 2, $lte: 3 }
            }
            if (option.where["doors"] == "4-5") {
                option.where["doors"] = { $gte: 4, $lte: 5 }
            }
        }
        const carSellFind = await this.carSellRepo.find(option)
        const result = await Promise.all(carSellFind.docs.map(async item => {
            const [bodyType, driveTrain, cylinders, fuelType, userSell, make, model, trim, featuresCar] = await Promise.all([
                this.bodyTypeRepo.findById(item.bodyTypeId),
                this.driveTrainRepo.findById(item.driveTrainId),
                this.cylindersRepo.findById(item.cylindersId),
                this.fuelTypeRepo.findById(item.fuelTypeId),
                this.userRepo.findById(item.userSellId),
                this.makeRepo.findById(item.makeId),
                this.modelRepo.findById(item.modelId),
                this.trimRepo.findById(item.trimId),
                Promise.all(item.featuresId.map(async id => {
                    const featuresItem = await this.carFeatureRepo.findById(id)
                    return featuresItem
                }))
            ])
            Object.assign(item._doc, {
                bodyType,
                driveTrain,
                cylinders,
                fuelType,
                userSell,
                make,
                model,
                trim,
                featuresCar
            })
            return item
        }))
        if (option.where["makeId"]) {
            if (Array.isArray(option.where["makeId"])) {
                const make = await Promise.all(option.where["makeId"].map(async item => {
                    const makeItem = await this.makeRepo.findById(item)
                    filter.push({
                        makeId: item,
                        label: makeItem.name
                    })
                }))
            }
            else {
                const makeItem = await this.makeRepo.findById(option.where["makeId"])
                filter.push({
                    makeId: option.where["makeId"],
                    label: makeItem.name
                })
            }
        }
        if (option.where["modelId"]) {
            if (Array.isArray(option.where["modelId"])) {
                const model = await Promise.all(option.where["modelId"].map(async item => {
                    const modelItem = await this.modelRepo.findById(item)
                    filter.push({
                        modelId: item,
                        label: modelItem.name
                    })
                }))
            }
            else {
                const modelItem = await this.modelRepo.findById(option.where["modelId"])
                filter.push({
                    modelId: option.where["modelId"],
                    label: modelItem.name
                })
            }
        }
        if (option.where["trimId"]) {
            if (Array.isArray(option.where["trimId"])) {
                const trim = await Promise.all(option.where["trimId"].map(async item => {
                    const trimItem = await this.trimRepo.findById(item)
                    filter.push({
                        trimId: item,
                        label: trimItem.name
                    })
                }))
            }
            else {
                const trimItem = await this.trimRepo.findById(option.where["trimId"])
                filter.push({
                    trimId: option.where["trimId"],
                    label: trimItem.name
                })
            }
        }
        if (option.where["bodyTypeId"]) {
            if (Array.isArray(option.where["bodyTypeId"])) {
                const bodyType = await Promise.all(option.where["bodyTypeId"].map(async item => {
                    const bodyTypeItem = await this.bodyTypeRepo.findById(item)
                    filter.push({
                        bodyTypeId: item,
                        label: bodyTypeItem.name
                    })
                }))
            }
            else {
                const bodyTypeItem = await this.bodyTypeRepo.findById(option.where["bodyTypeId"])
                filter.push({
                    bodyTypeId: option.where["bodyTypeId"],
                    label: bodyTypeItem.name
                })
            }
        }
        if (option.where["driveTrainId"]) {
            if (Array.isArray(option.where["driveTrainId"])) {
                const driveTrain = await Promise.all(option.where["driveTrainId"].map(async item => {
                    const driveTrainItem = await this.driveTrainRepo.findById(item)
                    filter.push({
                        driveTrainId: item,
                        label: driveTrainItem.name
                    })
                }))
            }
            else {
                const driveTrainItem = await this.driveTrainRepo.findById(option.where["driveTrainId"])
                filter.push({
                    driveTrainId: option.where["driveTrainId"],
                    label: driveTrainItem.name
                })
            }
        }
        if (option.where["cylindersId"]) {
            if (Array.isArray(option.where["cylindersId"])) {
                const cylinder = await Promise.all(option.where["cylindersId"].map(async item => {
                    const cylinderItem = await this.cylindersRepo.findById(item)
                    filter.push({
                        cylindersId: item,
                        label: cylinderItem.name
                    })
                }))
            }
            else {
                const cylinderItem = await this.cylindersRepo.findById(option.where["cylindersId"])
                filter.push({
                    cylindersId: option.where["cylindersId"],
                    label: cylinderItem.name
                })
            }
        }
        if (option.where["fuelTypeId"]) {
            if (Array.isArray(option.where["fuelTypeId"])) {
                const fuelType = await Promise.all(option.where["fuelTypeId"].map(async item => {
                    const fuelTypeItem = await this.fuelTypeRepo.findById(item)
                    filter.push({
                        fuelTypeId: item,
                        label: fuelTypeItem.name
                    })
                }))
            }
            else {
                const fuelTypeItem = await this.fuelTypeRepo.findById(option.where["fuelTypeId"])
                filter.push({
                    fuelTypeId: option.where["fuelTypeId"],
                    label: fuelTypeItem.name
                })
            }
        }
        if (option.where["featuresId"]) {
            if (Array.isArray(option.where["featuresId"])) {
                const feature = await Promise.all(option.where["featuresId"].map(async item => {
                    const featureItem = await this.carFeatureRepo.findById(item)
                    filter.push({
                        featuresId: item,
                        label: featureItem.name
                    })
                }))
            }
            else {
                const featureItem = await this.carFeatureRepo.findById(option.where["featuresId"])
                filter.push({
                    featuresId: option.where["featuresId"],
                    label: featureItem.name
                })
            }
        }
        if (queryCarSellDto.nameCar) {
            filter.push({
                nameCar: queryCarSellDto.nameCar,
                label: queryCarSellDto.nameCar
            })
        }
        if (queryCarSellDto.engineSize) {
            filter.push({
                engineSize: queryCarSellDto.engineSize,
                label: queryCarSellDto.engineSize
            })
        }
        if (queryCarSellDto.mileage) {
            filter.push({
                mileage: queryCarSellDto.mileage,
                label: queryCarSellDto.mileage
            })
        }
        if (queryCarSellDto.yearOfManufacture) {
            filter.push({
                yearOfManufacture: queryCarSellDto.yearOfManufacture,
                label: queryCarSellDto.yearOfManufacture
            })
        }
        if (queryCarSellDto.priceBuy) {
            filter.push({
                priceBuy: queryCarSellDto.priceBuy,
                label: queryCarSellDto.priceBuy
            })
        }
        if (queryCarSellDto.gearBox) {
            if (Array.isArray(queryCarSellDto.gearBox)) {
                const gearBox = queryCarSellDto.gearBox.map(item => {
                    filter.push({
                        gearBox: item,
                        label: item
                    })
                })
            }
            else {
                filter.push({
                    gearBox: queryCarSellDto.gearBox,
                    label: queryCarSellDto.gearBox
                })
            }
        }
        if (queryCarSellDto.color) {
            if (Array.isArray(queryCarSellDto.color)) {
                const color = queryCarSellDto.color.map(item => {
                    filter.push({
                        color: item,
                        label: item
                    })
                })
            }
            else {
                filter.push({
                    color: queryCarSellDto.color,
                    label: queryCarSellDto.color
                })
            }
        }
        if (queryCarSellDto.seats) {
            if (Array.isArray(queryCarSellDto.seats)) {
                const seats = queryCarSellDto.seats.map(item => {
                    filter.push({
                        seats: item,
                        label: item
                    })
                })
            }
            else {
                filter.push({
                    seats: queryCarSellDto.seats,
                    label: queryCarSellDto.seats
                })
            }
        }
        if (queryCarSellDto.doors) {
            if (Array.isArray(queryCarSellDto.doors)) {
                const doors = queryCarSellDto.doors.map(item => {
                    filter.push({
                        doors: item,
                        label: item
                    })
                })
            }
            else {
                filter.push({
                    doors: queryCarSellDto.doors,
                    label: queryCarSellDto.doors,
                })
            }
        }
        Object.assign(carSellFind, {
            filter
        })

        return carSellFind
    }
    async update(id, body: UpdateCarSellDto) {
        const { bodyTypeId, driveTrainId, cylindersId, makeId, modelId, trimId, fuelTypeId, featuresId, userSellId } = body
        const [existingCarSell, existingBodyType, existingDriveTrain, existingCylinders, existingMake, existingModel, existingTrim, existingFuelType, existingUser] = await Promise.all([
            this.carSellRepo.findById(id),
            this.bodyTypeRepo.findById(bodyTypeId),
            this.driveTrainRepo.findById(driveTrainId),
            this.cylindersRepo.findById(cylindersId),
            this.makeRepo.findById(makeId),
            this.modelRepo.findById(modelId),
            this.trimRepo.findById(trimId),
            this.fuelTypeRepo.findById(fuelTypeId),
            this.userRepo.findById(userSellId)
        ])
        if (!existingCarSell) throw new HttpException("Not found car sell", HttpStatus.BAD_REQUEST)
        if (makeId) {
            if (!existingMake) throw new HttpException("Not Found Make", HttpStatus.BAD_REQUEST)
        }
        if (modelId) {
            if (!existingModel) throw new HttpException("Not Found Model", HttpStatus.BAD_REQUEST)
        }
        if (trimId) {
            if (!existingTrim) throw new HttpException("Not Found Trim", HttpStatus.BAD_REQUEST)
        }
        if (bodyTypeId) {
            if (!existingBodyType) throw new HttpException("Not Found BodyType", HttpStatus.BAD_REQUEST)
        }
        if (driveTrainId) {
            if (!existingDriveTrain) throw new HttpException("Not Found DriveTrain", HttpStatus.BAD_REQUEST)
        }
        if (cylindersId) {
            if (!existingCylinders) throw new HttpException("Not Found Cylinders", HttpStatus.BAD_REQUEST)
        }

        if (fuelTypeId) {
            if (!existingFuelType) throw new HttpException("Not Found FuelType", HttpStatus.BAD_REQUEST)
        }
        if (featuresId) {
            await Promise.all(featuresId.map(async id => {
                const existingCarFeature = await this.carFeatureRepo.findById(id)
                if (!existingCarFeature) throw new HttpException(`Not found this carFeatureId ${id}`, HttpStatus.BAD_REQUEST)
            }))
        }
        if (userSellId) {
            if (!existingUser) throw new HttpException("Not Found UserSell", HttpStatus.BAD_REQUEST)
        }
        return await this.carSellRepo.update(id, body)
    }
    async createOffer(input) {
        const { plate, firebaseUser } = input
        const user = await this.userRepo.findOne({ firId: firebaseUser.uid })
        const log = await this.logRepo.find({
            pagination: { page: undefined, size: undefined },
            where: { method: 'POST', path: '/carsell/offer', "body.firebaseUser.uid": firebaseUser.uid, createdAt: { $gte: moment().startOf('day').toISOString(), $lte: moment().endOf('day').toISOString() } },
            orderBy: { createdAt: -1 }
        })
        if (log.totalCount > 3) return {}
        const existingCarSell = await this.get({ $or: [{ plate: plate }, { vin: plate }], expiresOn: { $gte: moment() } })
        if (existingCarSell.totalCount > 0) {
            return existingCarSell.docs[0]
        }
        else {
            const requestReport = {
                method: "GET",
                url: `${process.env.URL_CAR_JAM_REPORT}:create?key=${process.env.API_KEY_CAR_JAM}&plate=${plate}`
            }
            const responeReport = await axios(requestReport)
                .then(async res => {
                    const getReport = {
                        method: "GET",
                        url: `${process.env.URL_CAR_JAM_REPORT}:get?key=${process.env.API_KEY_CAR_JAM}&ref=${res.data.ref}`
                    }
                    const result = await axios(getReport)
                    return result.data
                })
                .catch(err => {
                    console.log(err)
                    throw new HttpException(err.response.data, HttpStatus.BAD_REQUEST)
                })
            const requestValuation = {
                method: "GET",
                url: `${process.env.URL_CAR_JAM_VALUATION}?key=${process.env.API_KEY_CAR_JAM}&plate=${plate}`
            }
            const responeValuation = await axios(requestValuation)
                .then(res => {
                    return res.data
                })
                .catch(err => {
                    throw new HttpException(err.response.data, HttpStatus.BAD_REQUEST)
                })
            return await this.carSellRepo.insert({ checkCarInforReport: responeReport, checkCarInforValuation: responeValuation, status: 0, expiresOn: moment().add(9, 'months'), plate: responeValuation.plate, vin: responeValuation.vin, userSellId: user.id })

        }
    }
}