import { forwardRef, MiddlewareConsumer, Module, NestModule, RequestMethod } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { AuthMiddleware } from "common/middleware/auth";
import { BodyTypeModule } from "src/bodytype/bodytype.module";
import { CarFeatureModule } from "src/carfeature/carfeature.module";
import { CylindersModule } from "src/cylinders/cylinders.module";
import { DriveTrainModule } from "src/drivetrain/drivetrain.module";
import { FuelTypeModule } from "src/fueltype/fueltype.module";
import { LogModule } from "src/log/log.module";
import { MakeModule } from "src/make/make.module";
import { ModelModule } from "src/model/model.module";
import { TrimModule } from "src/trim/trim.module";
import { UserModule } from "src/user/user.module";
import { UserRepository } from "src/user/user.repository";
import { CarSellController } from "./carsell.controller";
import { CarSell, CarSellSchema } from "./carsell.entity";
import { CarSellRepository } from "./carsell.repository";
import { CarSelleService } from "./carsell.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: CarSell.name, schema: CarSellSchema }]),
        BodyTypeModule,
        CarFeatureModule,
        CylindersModule,
        DriveTrainModule,
        FuelTypeModule,
        MakeModule,
        ModelModule,
        TrimModule,
        LogModule,
        forwardRef(() => UserModule)
    ],
    controllers: [CarSellController],
    providers: [CarSellRepository, CarSelleService],
    exports: [CarSellRepository]
})
export class CarSellModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            .exclude({ path: 'carsell', method: RequestMethod.GET },
                { path: 'carsell/:id', method: RequestMethod.GET })
            .forRoutes(CarSellController)
    }
}