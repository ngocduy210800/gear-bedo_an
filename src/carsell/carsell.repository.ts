import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { CarSell, CarSellDocument } from "./carsell.entity";
import { QueryCarSellDto } from "./dto/query-carsell.dto";
import { InjectModel } from "@nestjs/mongoose";

@Injectable()
export class CarSellRepository extends AbstractMongodbRepository<CarSell> {
    constructor(
        @InjectModel(CarSell.name) private carSellModel: Model<CarSellDocument>
    ) {
        super(carSellModel)
    }
    async mapOptionsQuery(queryCarSellDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, nameCar, yearOfManufacture, action
        , expiresOn, priceBuy, offerSell, taxRate, status, updateBy, userSellId, bodyTypeId, color, cylindersId, 
        doors, driveTrainId, engineSize, featuresId, fuelTypeId, gearBox, makeId, modelId, trimId, mileage, seats, fuelEconomyCity,
         fuelEconomyHighway, numberOfAccidents, numberOfKeys, numberOfOwners, plate, vin,$or } = queryCarSellDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (nameCar) { options.where = { ...options.where, nameCar: { $regex: `${nameCar}`, $options: 'i' } } }
        if (yearOfManufacture) { options.where = { ...options.where, yearOfManufacture } }
        if (action) { options.where = { ...options.where, action } }
        if (priceBuy) { options.where = { ...options.where, priceBuy } }
        if (offerSell) { options.where = { ...options.where, offerSell } }
        if (taxRate) { options.where = { ...options.where, taxRate } }
        if (expiresOn) { options.where = { ...options.where, expiresOn } }
        if (status) { options.where = { ...options.where, status } }
        if (updateBy) { options.where = { ...options.where, updateBy } }
        if (userSellId) { options.where = { ...options.where, userSellId } }
        if (bodyTypeId) { options.where = { ...options.where, bodyTypeId } }
        if (color) { options.where = { ...options.where, color } }
        if (cylindersId) { options.where = { ...options.where, cylindersId } }
        if (doors) { options.where = { ...options.where, doors } }
        if (driveTrainId) { options.where = { ...options.where, driveTrainId } }
        if (engineSize) { options.where = { ...options.where, engineSize } }
        if (featuresId) { options.where = { ...options.where, featuresId } }
        if (fuelTypeId) { options.where = { ...options.where, fuelTypeId } }
        if (gearBox) { options.where = { ...options.where, gearBox } }
        if (mileage) { options.where = { ...options.where, mileage } }
        if (seats) { options.where = { ...options.where, seats } }
        if (makeId) { options.where = { ...options.where, makeId } }
        if (modelId) { options.where = { ...options.where, modelId } }
        if (trimId) { options.where = { ...options.where, trimId } }
        if (numberOfAccidents) { options.where = { ...options.where, trimId } }
        if (numberOfKeys) { options.where = { ...options.where, trimId } }
        if (numberOfOwners) { options.where = { ...options.where, trimId } }
        if (fuelEconomyCity) { options.where = { ...options.where, fuelEconomyCity } }
        if (fuelEconomyHighway) { options.where = { ...options.where, fuelEconomyHighway } }
        if (plate) { options.where = { ...options.where, plate } }
        if (vin) { options.where = { ...options.where, vin } }
        if ($or) { options.where = { ...options.where, $or } }
        return options;
    }

}