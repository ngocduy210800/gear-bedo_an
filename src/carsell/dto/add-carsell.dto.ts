import { ApiProperty } from "@nestjs/swagger";
import { String } from "aws-sdk/clients/acm";
import { Type } from "class-transformer";
import { IsEnum, IsNotEmpty, IsNumber, IsObject, IsOptional, IsString, ValidateNested, } from "class-validator";
import { GREAR_BOX, STATUS_CAR_SELL } from "../constant/constant";
class Images {
    @IsOptional()
    @IsString({ each: true })
    @ApiProperty({ required: false, description: "link anh xe avatar" })
    avatarUrl?: string

    @IsOptional()
    @ApiProperty({ required: false, description: "link anh 360" })
    @IsString({ each: true })
    images360?: string[]

    @IsOptional()
    @ApiProperty({ required: false, description: "link anh xe basic" })
    @IsString({ each: true })
    basic?: string[]
}
export class PickUpInfor {
    @IsNotEmpty()
    @ApiProperty({ type: "object", required: true })
    garage: object

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty({ required: true, description: "số tiền khách phải đóng, thường là free" })
    price: number
}
export class DropOffInfor {
    @IsOptional()
    @ApiProperty({ required: false, type: "object", description: "thông tin garage mà mình mang xe về " })
    garage: object

    @IsNotEmpty()
    @ApiProperty({ required: true, type: "object", description: "địa chỉ mang xe đến lấy " })
    address: object

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty({ required: true, description: "số tiền khách phải đóng" })
    price: number
}
class Item {
    @IsNotEmpty()
    @ApiProperty()
    name: string

    @IsOptional()
    @ApiProperty({ required: false })
    description: string

    @IsNotEmpty()
    @ApiProperty()
    status: String
}

class ListItem {
    @IsNotEmpty()
    @Type(() => Item)
    @ValidateNested({ each: true })
    @ApiProperty({ type: [Item], required: true })
    items: Item[]

    @IsNotEmpty()
    @ApiProperty()
    status: string
}
class Repost {
    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    body: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    stationaryRoadTest: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    engine: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    suspension: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    steering: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    lights: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    underbody: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    tiresBrakes: ListItem

    @IsOptional()
    @Type(() => ListItem)
    @ValidateNested({ each: true })
    @ApiProperty({ type: ListItem, required: true })
    drivingRoadTest: ListItem
}
export class AddCarSellDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({ description: "ten xe" })
    nameCar: string

    @IsOptional()
    @ApiProperty({ required: false, description: "biển số xe" })
    plate: string

    @IsOptional()
    @ApiProperty({ required: false, description: "mã khung xe" })
    vin: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ description: "năm sản xuất" })
    yearOfManufacture: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ description: "số dặm" })
    mileage: number

    @IsOptional()
    @IsEnum(GREAR_BOX)
    @ApiProperty({ enum: GREAR_BOX })
    gearBox: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ description: "engineSize" })
    engineSize: number

    @IsOptional()
    @IsString()
    @ApiProperty()
    color: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    bodyTypeId: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    driveTrainId: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    cylindersId: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    makeId: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    modelId: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    trimId: string

    @IsOptional()
    @IsString()
    @ApiProperty()
    fuelTypeId: string

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    doors: number

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    seats: number

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    numberOfAccidents: number

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    numberOfKeys: number

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    numberOfOwners: number

    @IsOptional()
    @IsObject()
    @Type(() => Images)
    @ValidateNested({ each: true })
    @ApiProperty({ required: false, type: Images })
    images: Images

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    fuelEconomyCity: number

    @IsOptional()
    @IsNumber()
    @ApiProperty()
    fuelEconomyHighway: number

    @IsOptional()
    @ApiProperty()
    featuresId?: string[]

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    userSellId: string

    @IsOptional()
    @IsEnum(STATUS_CAR_SELL)
    @ApiProperty({
        description: ' -1: khach hang ko ban nua,0: moi khoi tao,1: da nhan xe,2:da check xe, 3: hoan tat thu tuc ban xe,4: xe san sang ban,5: xe da ban'
    })
    status: number

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    action: string

    @IsOptional()
    @ApiProperty({ required: false })
    updateBy: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    priceBuy: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    offerSell: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    taxRate: number

    @IsOptional()
    @ApiProperty({ required: false })
    tellUsAboutYourself: object

    @IsOptional()
    @ApiProperty({ required: false, type: Date })
    expiresOn: Date

    @IsOptional()
    @Type(() => DropOffInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: DropOffInfor, required: false })
    dropOffInfor: DropOffInfor

    @IsOptional()
    @Type(() => PickUpInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: PickUpInfor, required: false })
    pickUpInfor: PickUpInfor

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    checkCarInforReport: object

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    checkCarInforValuation: object

    @IsOptional()
    @ApiProperty({ required: false })
    exterior: string

    @IsOptional()
    @ApiProperty({ required: false })
    interior: string

    @IsOptional()
    @ApiProperty({ required: false })
    aspiration: string

    @IsOptional()
    @ApiProperty({ required: false })
    engineDisplacement: String

    @IsOptional()
    @ApiProperty({ required: false })
    horsepower: string

    @IsOptional()
    @ApiProperty({ required: false })
    torque: string

    @IsOptional()
    @ApiProperty({ required: false })
    fuelTankCapacity: string

    @IsOptional()
    @ApiProperty({ required: false })
    cargoCapacity: string

    @IsOptional()
    @ApiProperty({ required: false })
    frontLegRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    frontHeadRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    backLegRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    backHeadRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    point: number

    @IsOptional()
    @Type(() => Repost)
    @ValidateNested({ each: true })
    @ApiProperty({ type: Repost, required: true })
    report: Repost

    @IsOptional()
    @ApiProperty({ required: false })
    modifications: boolean

    @IsOptional()
    @ApiProperty({ required: false })
    personalisedPlate: boolean

    @IsOptional()
    @ApiProperty({ required: false })
    outstandingFinance: boolean

    @IsOptional()
    @ApiProperty({ required: false })
    serviceHistory: string

    @IsOptional()
    @ApiProperty({ required: false })
    condition: string
}

