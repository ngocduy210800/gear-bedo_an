import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsEnum, isJSON, IsNotEmpty, IsNumber, IsOptional, IsString, } from "class-validator";
import * as moment from 'moment'
import { GREAR_BOX, STATUS_CAR_SELL } from "../constant/constant";

export class QueryCarSellDto {
    @IsOptional()
    @ApiProperty({ required: false })
    public id?: string;

    @IsOptional()
    @ApiProperty({ required: false, default: 1 })
    public page?: Number;

    @IsOptional()
    @ApiProperty({ required: false, default: 100 })
    public size?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    @ApiProperty({ required: false, type: "any", description: "de sort", example: "orderBy:{ name: 1 }" })
    public orderBy?: object;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "createdAt={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public createdAt?: object;

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    nameCar?: string

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "yearOfManufacture={'$gte':'yyyy','$lte':'yyyy'}" })
    public yearOfManufacture?: any;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false,type: "any", description: "co the query gte,lte", example: "mileage={'$gte':'number','$lte':'number'}" })
    public mileage?: any;

    @IsOptional()
    @ApiProperty({ enum: GREAR_BOX })
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    gearBox?: string

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false,type: "any", description: "co the query gte,lte", example: "engineSize={'$gte':'number','$lte':'number'}" })
    engineSize?: any

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    color?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    bodyTypeId?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    driveTrainId?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    cylindersId?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    makeId?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    modelId?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    trimId?: string

    @IsOptional()
    @ApiProperty({ required: false })
    //@IsString()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    fuelTypeId?: string

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "doors={'$gte':'number','$lte':'number'}" })
    doors?: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "seats={'$gte':'number','$lte':'number'}" })
    seats?: any


    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "fuelEconomyCity={'$gte':'number','$lte':'number'}" })
    fuelEconomyCity?: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "fuelEconomyHighway={'$gte':'number','$lte':'number'}" })
    fuelEconomyHighway?: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "numberOfAccidents={'$gte':'number','$lte':'number'}" })
    numberOfAccidents?: any


    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "numberOfKeys={'$gte':'number','$lte':'number'}" })
    numberOfKeys?: any


    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "numberOfOwners={'$gte':'number','$lte':'number'}" })
    numberOfOwners?: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query mang", example: 'featuresId=["@parentId1","@parentId2"]' })
    featuresId?: any

    @IsOptional()
    @ApiProperty({ required: false })
    @IsString()
    userSellId: string

    @IsOptional()
    @ApiProperty({ required: false })
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    status: number

    @IsOptional()
    @IsString()
    action: string

    @IsOptional()
    @ApiProperty({ required: false })
    @IsString()
    updateBy: string

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "priceBuy={'$gte':'number','$lte':'number'}" })
    priceBuy: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "offerSell={'$gte':'number','$lte':'number'}" })
    offerSell: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "expiresOn={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    expiresOn: any

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query gte,lte", example: "taxRate={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    taxRate: any

    @IsOptional()
    @ApiProperty({ required: false })
    @IsString()
    plate: string

    @IsOptional()
    @ApiProperty({ required: false })
    @IsString()
    vin: string

}
