import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsOptional, IsString } from "class-validator"

export class OfferPlate {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true, description: "bien so xe hoac ma khung xe" })
    plate: string

    @IsOptional()
    firebaseUser: object
}