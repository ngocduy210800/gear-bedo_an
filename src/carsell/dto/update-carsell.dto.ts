import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { ValidateNested } from "class-validator";
import { IsEnum, IsNotEmpty, IsNumber, IsObject, IsOptional, IsString, } from "class-validator";
import { GREAR_BOX, STATUS_CAR_SELL } from "../constant/constant";
class Images {
    @IsOptional()
    @IsString({ each: true })
    @ApiProperty({ required: false, description: "link anh xe avatar" })
    avatarUrl?: string

    @IsOptional()
    @ApiProperty({ required: false, description: "link anh 360" })
    @IsString({ each: true })
    images360?: string[]

    @IsOptional()
    @ApiProperty({ required: false, description: "link anh xe basic" })
    @IsString({ each: true })
    basic?: string[]
}
export class PickUpInfor {
    @IsNotEmpty()
    @ApiProperty({ required: true, type: "object", description: "thông tin garage người ta mang đến" })
    garage: object

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty({ required: true, description: "số tiền khách phải đóng, thường là free" })
    price: number
}
export class DropOffInfor {
    @IsOptional()
    @ApiProperty({ required: false, type: "object", description: "thông tin garage mà mình mang xe về " })
    garage: object

    @IsNotEmpty()
    @ApiProperty({ required: true, type: "object", description: "địa chỉ mang xe đến lấy " })
    address: object

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: true, description: "số tiền khách phải đóng" })
    price: number
}
export class UpdateCarSellDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    nameCar?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    yearOfManufacture?: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    mileage?: number

    @IsOptional()
    @IsEnum(GREAR_BOX)
    @ApiProperty({ enum: GREAR_BOX, required: false })
    gearBox?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    engineSize?: number

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    color?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    bodyTypeId?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    driveTrainId?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    cylindersId?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    makeId?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    modelId?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    trimId?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    fuelTypeId?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    doors?: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    seats?: number

    @IsOptional()
    @ApiProperty({ required: false })
    featuresId?: string[]

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    userSellId?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    numberOfAccidents?: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    numberOfKeys?: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    numberOfOwners?: number

    @IsOptional()
    @IsEnum(STATUS_CAR_SELL)
    @ApiProperty({
        description: ' -1: khach hang ko ban nua,0: moi khoi tao,1: da nhan xe,2:da check xe, 3: hoan tat thu tuc ban xe,4: xe san sang ban,5: xe da ban'
    })
    status?: number

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    action?: string

    @IsOptional()
    @ApiProperty({ required: false })
    updateBy?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    priceBuy: number

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    offerSell: number

    @IsOptional()
    @ApiProperty({ required: false })
    tellUsAboutYourself: object

    @IsOptional()
    @ApiProperty({ required: false })
    expiresOn: Date

    @IsOptional()
    @Type(() => DropOffInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: DropOffInfor, required: false })
    dropOffInfor: DropOffInfor

    @IsOptional()
    @Type(() => PickUpInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: PickUpInfor, required: false })
    pickUpInfor: PickUpInfor

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    checkCarInforReport: object

    @IsOptional()
    @IsObject()
    @ApiProperty({ required: false,type:Images })
    images: Images

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    taxRate: number

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    checkCarInforValuation: object

    @IsOptional()
    @ApiProperty({ required: false })
    plate: string

    @IsOptional()
    @ApiProperty({ required: false })
    vin: string

    @IsOptional()
    @ApiProperty({ required: false })
    exterior: string

    @IsOptional()
    @ApiProperty({ required: false })
    interior: string

    @IsOptional()
    @ApiProperty({ required: false })
    aspiration: string

    @IsOptional()
    @ApiProperty({ required: false })
    engineDisplacement: String

    @IsOptional()
    @ApiProperty({ required: false })
    horsepower: string

    @IsOptional()
    @ApiProperty({ required: false })
    torque: string

    @IsOptional()
    @ApiProperty({ required: false })
    fuelTankCapacity: string

    @IsOptional()
    @ApiProperty({ required: false })
    cargoCapacity: string

    @IsOptional()
    @ApiProperty({ required: false })
    frontLegRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    frontHeadRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    backLegRoom: string

    @IsOptional()
    @ApiProperty({ required: false })
    backHeadRoom:string

    @IsOptional()
    @ApiProperty({ required: false })
    point: number

    @IsOptional()
    @ApiProperty({ required: false })
    report: object

    @IsOptional()
    @ApiProperty({ required: false })
    modifications: boolean

    @IsOptional()
    @ApiProperty({ required: false })
    personalisedPlate: boolean

    @IsOptional()
    @ApiProperty({ required: false })
    outstandingFinance: boolean

    @IsOptional()
    @ApiProperty({ required: false })
    serviceHistory: string
    
    @IsOptional()
    @ApiProperty({ required: false })
    condition:string
}
