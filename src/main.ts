import * as dotenv from 'dotenv';
dotenv.config();
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { HttpExceptionFilter } from 'common/decorators/http-exception-filters';
import { AppModule } from './app.module';
import * as admin from 'firebase-admin'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  //cap nhat thong tin firebase va cho vao env
  admin.initializeApp({
    credential: admin.credential.cert({
      clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
      privateKey: process.env.FIREBASE_PRIVATE_KEY
        ? process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/gm, "\n")
        : undefined,
      projectId: process.env.FIREBASE_PROJECTID
    } as Partial<admin.ServiceAccount>)
  });
  const config = new DocumentBuilder()
    .setTitle('Gear_be API')
    .addServer("/api")
    .setDescription('Gear_be API')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
  app.enableCors();
  await app.listen(3001);
}
bootstrap();
