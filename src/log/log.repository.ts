import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Log, LogDocument } from "./log.entity";
import { QueryLogDto } from "./dto/request/query-log.dto";

@Injectable()
export class LogRepository extends AbstractMongodbRepository<Log> {
    constructor(
        @InjectModel(Log.name) private logModel: Model<LogDocument>
    ) {
        super(logModel)
    }
    async mapOptionsQuery(queryLogDto: QueryLogDto) {
        const { page, size, id, createdAt, orderBy =  { createdAt: -1 }, method,path } = queryLogDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (method) { options.where = { ...options.where, method } }
        if (path) { options.where = { ...options.where, path } }
        return options;
    }
}