import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { LogService } from "./log.service";
import { QueryLogDto } from "./dto/request/query-log.dto";
import { ApiSecurity, ApiTags } from "@nestjs/swagger";
@Controller("/log")
@ApiTags("Log")
export class LogController {
    constructor(private logService: LogService) { }
    @Get()
    get(@Query() querylogDto: QueryLogDto) {
        return this.logService.get(querylogDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.logService.getById(id)
    }
}