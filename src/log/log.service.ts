import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { LogRepository } from "./log.repository";


@Injectable()
export class LogService {
    constructor(
        private logRepo: LogRepository
    ) { }
    async create(input) {
        return await this.logRepo.insert(input)
    }
    async update(id, body) {
        const existingLog = await this.logRepo.findById(id)
        if (!existingLog) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        return await this.logRepo.update(id, body)
    }
    async get(query) {
        const option = await this.logRepo.mapOptionsQuery(query)
        return await this.logRepo.find(option)
    }
    async getById(id) {
        const log = await this.logRepo.findById(id)
        if (!log) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return log
    }
    async delete(id) {
        const log = await this.logRepo.findById(id)
        if (!log) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.logRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}