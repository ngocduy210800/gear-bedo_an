import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type LogDocument = Log & Document;

@Schema({ timestamps: true })
export class Log {
    @Prop()
    path: string

    @Prop()
    method: string

    @Prop({ type: "object" })
    param: object

    @Prop({ type: "object" })
    query: object

    @Prop({ type: "object" })
    header: object

    @Prop({ type: "object" })
    body: object

    @Prop({ type: "object" })
    output: object

}
export const LogSchema = SchemaFactory.createForClass(Log);