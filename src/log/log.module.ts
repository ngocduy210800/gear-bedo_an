import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { LogController } from "./log.controller";
import { Log, LogSchema } from "./log.entity";
import { LogRepository } from "./log.repository";
import { LogService } from "./log.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: Log.name, schema: LogSchema }])],
    providers: [LogService,LogRepository],
    controllers:[LogController],
    exports:[LogService,LogRepository]
})
export class LogModule { }