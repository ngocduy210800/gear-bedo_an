import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type FormRequestDocument = FormRequest & Document;

@Schema({ timestamps: true })
export class FormRequest {
    @Prop()
    userId: string

    @Prop()
    vin: string

    @Prop()
    firstName: string

    @Prop()
    lastName: string

    @Prop()
    email: string

    @Prop()
    phoneNumber: string

    @Prop({ default: 0 })
    status: number
}
export const FormRequestSchema = SchemaFactory.createForClass(FormRequest);