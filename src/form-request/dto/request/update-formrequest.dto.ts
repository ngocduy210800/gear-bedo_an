import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsOptional, IsString, } from "class-validator";

export class UpdateFormRequestDto {
    @IsOptional()
    @ApiProperty({ required: true })
    userId: string

    @IsOptional()
    @ApiProperty({ required: true })
    vin: string

    @IsOptional()
    @ApiProperty({ required: false })
    firstName: string

    @IsOptional()
    @ApiProperty({ required: false })
    lastName: string

    @IsOptional()
    @ApiProperty({ required: true })
    email: string

    @IsOptional()
    @ApiProperty({ required: true })
    phoneNumber: string

    @IsOptional()
    @ApiProperty({ required: true,description:"status 0 là mới khởi tạo, 1 là đã giải quyết xong" })
    @IsEnum([0, 1])
    status: number
}
