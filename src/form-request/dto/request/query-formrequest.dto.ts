import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsEnum, isJSON, IsNotEmpty, IsOptional, IsString, } from "class-validator";
import * as moment from 'moment'

export class QueryFormRequestDto {
    @IsOptional()
    @ApiProperty({ required: false })
    public id?: string;

    @IsOptional()
    @ApiProperty({ required: false, default: 1 })
    public page?: Number;

    @IsOptional()
    @ApiProperty({ required: false, default: 100 })
    public size?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    @ApiProperty({ required: false, type: "object", description: "de sort", example: "orderBy:{ name: 1 }" })
    public orderBy?: object;

    @IsNotEmpty()
    @ApiProperty({ required: true })
    userId: string

    @IsNotEmpty()
    @ApiProperty({ required: true })
    vin: string

    @IsOptional()
    @ApiProperty({ required: false })
    firstName: string

    @IsOptional()
    @ApiProperty({ required: false })
    lastName: string

    @IsNotEmpty()
    @ApiProperty({ required: true })
    email: string

    @IsNotEmpty()
    @ApiProperty({ required: true })
    phoneNumber: string

    @IsOptional()
    @ApiProperty({ required: true,description:"status 0 là mới khởi tạo, 1 là đã giải quyết xong" })
    @IsEnum([0, 1])
    status: number
}
