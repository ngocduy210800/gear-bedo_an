import {  Module } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { FormRequestController} from "./form-request.controller";
import { FormRequest, FormRequestSchema } from "./form-request.entity";
import { FormRequestRepository } from "./form-request.repository";
import { FormRequestService } from "./form-request.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: FormRequest.name, schema: FormRequestSchema }]),
    ],
    controllers: [FormRequestController],
    providers: [FormRequestRepository, FormRequestService],
    exports: [FormRequestRepository]
})
export class FormRequestModule { }