import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { FormRequest, FormRequestDocument } from "./form-request.entity";
import { QueryFormRequestDto } from "./dto/request/query-formrequest.dto";

@Injectable()
export class FormRequestRepository extends AbstractMongodbRepository<FormRequest> {
    constructor(
        @InjectModel(FormRequest.name) private FAQCategoryModel: Model<FormRequestDocument>
    ) {
        super(FAQCategoryModel)
    }
    async mapOptionsQuery(queryMakeDto: QueryFormRequestDto) {
        const { page, size, id, orderBy = { createdAt: -1 }, userId, email, firstName, lastName, phoneNumber, status, vin } = queryMakeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (userId) { options.where = { ...options.where, userId } }
        if (firstName) { options.where = { ...options.where, firstName } }
        if (lastName) { options.where = { ...options.where, lastName } }
        if (phoneNumber) { options.where = { ...options.where, phoneNumber } }
        if (status) { options.where = { ...options.where, status } }
        if (vin) { options.where = { ...options.where, vin } }
        if (email) { options.where = { ...options.where, email } }
        return options;
    }

}