import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryFormRequestDto } from "./dto/request/query-formrequest.dto";
import { UpdateFormRequestDto } from "./dto/request/update-formrequest.dto";
import { AddFormRequestDto } from "./dto/request/add-formrequest.dto";
import { FormRequestService } from "./form-request.service";
import { ApiTags } from "@nestjs/swagger";
@Controller("/form-request")
@ApiTags("form-request")
export class FormRequestController {
    constructor(private _service: FormRequestService) { }
    @Post()
    create(@Body() body: AddFormRequestDto) {
        return this._service.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateFormRequestDto) {
        return this._service.update(id, body)
    }
    @Get()
    get(@Query() queryMakeDto: QueryFormRequestDto) {
        return this._service.get(queryMakeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this._service.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this._service.delete(id)
    }
}