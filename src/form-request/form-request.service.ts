import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { FormRequestRepository } from "./form-request.repository";
import { AddFormRequestDto } from "./dto/request/add-formrequest.dto";
import { QueryFormRequestDto } from "./dto/request/query-formrequest.dto";
import { UpdateFormRequestDto } from "./dto/request/update-formrequest.dto";


@Injectable()
export class FormRequestService {
    constructor(
        private formRequestService: FormRequestRepository
    ) { }
    async create(input: AddFormRequestDto) {
        return await this.formRequestService.insert(input)
    }
    async update(id, body: UpdateFormRequestDto) {
        return await this.formRequestService.update(id, body)
    }
    async get(query: QueryFormRequestDto) {
        const option = await this.formRequestService.mapOptionsQuery(query)
        return await this.formRequestService.find(option)
    }
    async getById(id) {
        const faqCategory = await this.formRequestService.findById(id)
        if (!faqCategory) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return faqCategory
    }
    async delete(id) {
        const faqCategory = await this.formRequestService.findById(id)
        if (!faqCategory) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.formRequestService.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}