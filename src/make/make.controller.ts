import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryMakeDto } from "./dto/request/query-make.dto";
import { UpdateMakeDto } from "./dto/request/update-make.dto";
import { AddMakeDto } from "./dto/request/add-make.dto";
import { MakeService } from "./make.service";
import { ApiTags } from "@nestjs/swagger";
@Controller("/make")
@ApiTags("Make")
export class MakeController {
    constructor(private makeService: MakeService) { }
    @Post()
    create(@Body() body: AddMakeDto) {
        return this.makeService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateMakeDto) {
        return this.makeService.update(id, body)
    }
    @Get()
    get(@Query() queryMakeDto: QueryMakeDto) {
        return this.makeService.get(queryMakeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.makeService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.makeService.delete(id)
    }
}