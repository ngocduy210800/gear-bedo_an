import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Make, MakeDocument } from "./make.entity";
import { QueryMakeDto } from "./dto/request/query-make.dto";

@Injectable()
export class MakeRepository extends AbstractMongodbRepository<Make> {
    constructor(
        @InjectModel(Make.name) private makeModel: Model<MakeDocument>
    ) {
        super(makeModel)
    }
    async mapOptionsQuery(queryMakeDto: QueryMakeDto) {
        const { page, size, id, orderBy =  { createdAt: -1 }, name, createdAt } = queryMakeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (createdAt) { options.where = { ...options.where, createdAt } }
        return options;
    }

}