import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { MakeController } from "./make.controller";
import { Make, MakeSchema } from "./make.entity";
import { MakeRepository } from "./make.repository";
import { MakeService } from "./make.service";



@Module({
    imports: [MongooseModule.forFeature([{ name: Make.name, schema: MakeSchema }])],
    controllers: [MakeController],
    providers: [MakeRepository, MakeService],
    exports: [MakeRepository]
})
export class MakeModule { }