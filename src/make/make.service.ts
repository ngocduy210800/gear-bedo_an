import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { MakeRepository } from "./make.repository";
import { AddMakeDto } from "./dto/request/add-make.dto";
import { QueryMakeDto } from "./dto/request/query-make.dto";
import { UpdateMakeDto } from "./dto/request/update-make.dto";


@Injectable()
export class MakeService {
    constructor(
        private makeRepo: MakeRepository
    ) { }
    async create(input: AddMakeDto) {
        const { name } = input
        const existingName = await this.makeRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.makeRepo.insert(input)
    }
    async update(id, body: UpdateMakeDto) {
        const { name } = body
        const existingMake = await this.makeRepo.findById(id)
        if (!existingMake) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.makeRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.makeRepo.update(id, body)
    }
    async get(query: QueryMakeDto) {
        const option = await this.makeRepo.mapOptionsQuery(query)
        return await this.makeRepo.find(option)
    }
    async getById(id) {
        const make = await this.makeRepo.findById(id)
        if (!make) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return make
    }
    async delete(id) {
        const make = await this.makeRepo.findById(id)
        if (!make) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.makeRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}