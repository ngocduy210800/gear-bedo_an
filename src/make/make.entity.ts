import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type MakeDocument = Make & Document;

@Schema({ timestamps: true })
export class Make {
    @Prop()
    name: string

    @Prop()
    description: string
}
export const MakeSchema = SchemaFactory.createForClass(Make);