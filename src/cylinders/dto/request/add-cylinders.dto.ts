import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString, } from "class-validator";

export class AddCylindersDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    name: string

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty()
    count: number

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string
}
