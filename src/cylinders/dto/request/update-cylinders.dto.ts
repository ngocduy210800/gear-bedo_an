import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString, } from "class-validator";

export class UpdateCylindersDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    count?: number

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string
}
