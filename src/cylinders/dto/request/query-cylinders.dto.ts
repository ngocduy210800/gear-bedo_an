import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { isJSON, IsNotEmpty, IsNumber, IsOptional, IsString, } from "class-validator";
import * as moment from 'moment'

export class QueryCylindersDto {
    @IsOptional()
    @ApiProperty({ required: false })
    public id?: string;

    @IsOptional()
    @ApiProperty({ required: false, default: 1 })
    public page?: Number;

    @IsOptional()
    @ApiProperty({ required: false, default: 100 })
    public size?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    @ApiProperty({ required: false, type: "object", description: "de sort", example: "orderBy:{ name: 1 }" })
    public orderBy?: object;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, description: "co the query tu ngay den ngay", example: "createdAt={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public createdAt?: object;

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

    @IsOptional()
    @IsNumber()
    @ApiProperty({ required: false })
    count?: number
}
