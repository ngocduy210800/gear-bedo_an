import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Cylinders, CylindersDocument } from "./cylinders.entity";
import { QueryCylindersDto } from "./dto/request/query-cylinders.dto";

@Injectable()
export class CylindersRepository extends AbstractMongodbRepository<Cylinders> {
    constructor(
        @InjectModel(Cylinders.name) private cylindersModel: Model<CylindersDocument>
    ) {
        super(cylindersModel)
    }
    async mapOptionsQuery(queryCylindersDto: QueryCylindersDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, name,count } = queryCylindersDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (count) { options.where = { ...options.where, count } }
        return options;
    }

}