import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CylindersRepository } from "./cylinders.repository";
import { AddCylindersDto } from "./dto/request/add-cylinders.dto";
import { QueryCylindersDto } from "./dto/request/query-cylinders.dto";
import { UpdateCylindersDto } from "./dto/request/update-cylinders.dto";


@Injectable()
export class CylindersService {
    constructor(
        private cylindersRepo: CylindersRepository
    ) { }
    async create(input: AddCylindersDto) {
        const { name } = input
        const existingName = await this.cylindersRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.cylindersRepo.insert(input)
    }
    async update(id, body: UpdateCylindersDto) {
        const { name } = body
        const existingCynlinders = await this.cylindersRepo.findById(id)
        if (!existingCynlinders) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.cylindersRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.cylindersRepo.update(id, body)
    }
    async get(query: QueryCylindersDto) {
        const option = await this.cylindersRepo.mapOptionsQuery(query)
        return await this.cylindersRepo.find(option)
    }
    async getById(id) {
        const cylinders = await this.cylindersRepo.findById(id)
        if (!cylinders) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return cylinders
    }
    async delete(id) {
        const cylinders = await this.cylindersRepo.findById(id)
        if (!cylinders) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.cylindersRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}