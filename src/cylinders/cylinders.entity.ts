import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type CylindersDocument = Cylinders & Document;

@Schema({ collection: "cylinders", timestamps: true })
export class Cylinders {
    @Prop()
    name: string

    @Prop()
    count: number

    @Prop()
    description: string

}
export const CylindersSchema = SchemaFactory.createForClass(Cylinders);