import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { CylindersService } from "./cylinders.service";
import { AddCylindersDto } from "./dto/request/add-cylinders.dto";
import { QueryCylindersDto } from "./dto/request/query-cylinders.dto";
import { UpdateCylindersDto } from "./dto/request/update-cylinders.dto";


@Controller("/cylinders")
@ApiTags("cylinders")
export class CylindersController {
    constructor(private cylindersService: CylindersService) { }
    @Post()
    create(@Body() body: AddCylindersDto) {
        return this.cylindersService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateCylindersDto) {
        return this.cylindersService.update(id, body)
    }
    @Get()
    get(@Query() queryCylindersDto: QueryCylindersDto) {
        return this.cylindersService.get(queryCylindersDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.cylindersService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.cylindersService.delete(id)
    }
}