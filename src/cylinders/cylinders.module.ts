import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { CylindersController } from "./cylinders.controller";
import { Cylinders, CylindersSchema } from "./cylinders.entity";
import { CylindersRepository } from "./cylinders.repository";
import { CylindersService } from "./cylinders.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: Cylinders.name, schema: CylindersSchema }])],
    controllers: [CylindersController],
    providers: [CylindersRepository, CylindersService],
    exports: [CylindersRepository]
})
export class CylindersModule { }