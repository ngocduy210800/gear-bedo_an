import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { ModelModule } from "src/model/model.module";
import { TrimController } from "./trim.controller";
import { Trim, TrimSchema } from "./trim.entity";
import { TrimRepository } from "./trim.repository";
import { TrimService } from "./trim.service";



@Module({
    imports: [
        MongooseModule.forFeature([{ name: Trim.name, schema: TrimSchema }]),
        ModelModule
    ],
    controllers: [TrimController],
    providers: [TrimRepository, TrimService],
    exports: [TrimRepository]
})
export class TrimModule { }