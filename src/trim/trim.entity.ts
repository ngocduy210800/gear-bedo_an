import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type TrimDocument = Trim & Document;

@Schema({ timestamps: true })
export class Trim {
    @Prop()
    name: string

    @Prop()
    description: string

    @Prop()
    modelId: string
}
export const TrimSchema = SchemaFactory.createForClass(Trim);