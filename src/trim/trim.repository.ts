import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Trim , TrimDocument } from "./trim.entity";
import { QueryTrimDto } from "./dto/request/query-trim.dto";

@Injectable()
export class TrimRepository extends AbstractMongodbRepository<Trim> {
    constructor(
        @InjectModel(Trim.name) private trimModel: Model<TrimDocument>
    ) {
        super(trimModel)
    }
    async mapOptionsQuery(queryModelDto: QueryTrimDto) {
        const { page, size, id, orderBy =  { createdAt: -1 }, name, createdAt, modelId } = queryModelDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (modelId) { options.where = { ...options.where, modelId } }
        if (createdAt) { options.where = { ...options.where, createdAt } }

        return options;
    }

}