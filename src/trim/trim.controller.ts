import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryTrimDto } from "./dto/request/query-trim.dto";
import { UpdateTrimDto } from "./dto/request/update-trim.dto";
import { AddTrimDto } from "./dto/request/add-trim.dto";
import { TrimService } from "./trim.service";
import { ApiTags } from "@nestjs/swagger";
@Controller("/trim")
@ApiTags("Trim")
export class TrimController {
    constructor(private trimService: TrimService) { }
    @Post()
    create(@Body() body: AddTrimDto) {
        return this.trimService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateTrimDto) {
        return this.trimService.update(id, body)
    }
    @Get()
    get(@Query() queryTrimDto: QueryTrimDto) {
        return this.trimService.get(queryTrimDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.trimService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.trimService.delete(id)
    }
}