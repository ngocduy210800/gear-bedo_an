import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { TrimRepository } from "./trim.repository";
import { AddTrimDto } from "./dto/request/add-trim.dto";
import { QueryTrimDto } from "./dto/request/query-trim.dto";
import { UpdateTrimDto } from "./dto/request/update-trim.dto";

import { ModelRepository } from "src/model/model.repository";


@Injectable()
export class TrimService {
    constructor(
        private trimRepo: TrimRepository,
        private modelRepo: ModelRepository
    ) { }
    async create(input: AddTrimDto) {
        const { name, modelId } = input
        const [existingName, existingModel] = await Promise.all([
            this.trimRepo.findOne({ name, modelId }),
            this.modelRepo.findById(modelId)
        ])
        if (existingName) throw new HttpException("Name with model already exists", HttpStatus.BAD_REQUEST)
        if (!existingModel) throw new HttpException("Not found modelId", HttpStatus.BAD_REQUEST)
        return await this.trimRepo.insert(input)
    }
    async update(id, body: UpdateTrimDto) {
        const { name, modelId } = body
        const existingTrim = await this.trimRepo.findById(id)
        if (!existingTrim) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.trimRepo.findOne({ name, modelId: modelId || existingTrim.modelId })
            if (existingName) throw new HttpException("Name with model already exists", HttpStatus.BAD_REQUEST)
        }
        if (modelId) {
            const existingModel = await this.modelRepo.findById(modelId)
            if (!existingModel) throw new HttpException("Not found modelId", HttpStatus.BAD_REQUEST)
        }
        return await this.trimRepo.update(id, body)
    }
    async get(query: QueryTrimDto) {
        const option = await this.trimRepo.mapOptionsQuery(query)
        const result=await this.trimRepo.find(option)
        await Promise.all(result.docs.map(async item => {
            const model = await this.modelRepo.findById(item.modelId)
            Object.assign(item._doc, {
                model
            })
            return item
        }))
        return result
    }
    async getById(id) {
        const trim = await this.trimRepo.findById(id)
        if (!trim) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const model = await this.modelRepo.findById(trim.modelId)
        Object.assign(trim._doc, {
            model
        })
        return trim
    }
    async delete(id) {
        const model = await this.trimRepo.findById(id)
        if (!model) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.trimRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}