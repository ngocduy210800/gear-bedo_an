import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddFuelTypeDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    name: string
    
    @IsNotEmpty()
    @IsBoolean()
    @ApiProperty()
    green: boolean
    
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

}
