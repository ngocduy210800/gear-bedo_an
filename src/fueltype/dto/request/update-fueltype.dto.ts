import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsOptional, IsString, } from "class-validator";

export class UpdateFuelTypeDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false})
    name?: string

    @IsOptional()
    @IsBoolean()
    @ApiProperty({ required: false})
    green?: boolean

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false})
    description?: string
}
