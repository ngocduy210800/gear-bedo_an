import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { FuelTypeController } from "./fueltype.controller";

import { FuelType, FuelTypeSchema } from "./fueltype.entity";
import { FuelTypeRepository } from "./fueltype.repository";
import { FuelTypeService } from "./fueltype.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: FuelType.name, schema: FuelTypeSchema }])],
    controllers: [FuelTypeController],
    providers: [FuelTypeRepository, FuelTypeService],
    exports: [FuelTypeRepository]
})
export class FuelTypeModule { }