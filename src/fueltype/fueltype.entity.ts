import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type FuelTypeDocument = FuelType & Document;

@Schema({ timestamps: true })
export class FuelType {
    @Prop()
    name: string

    @Prop()
    green: boolean
    
    @Prop()
    description: string

}
export const FuelTypeSchema = SchemaFactory.createForClass(FuelType);