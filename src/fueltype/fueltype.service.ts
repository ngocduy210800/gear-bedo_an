import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { FuelTypeRepository } from "./fueltype.repository";
import { AddFuelTypeDto } from "./dto/request/add-fueltype.dto";
import { QueryFuelTypeDto } from "./dto/request/query-fueltype.dto";
import { UpdateFuelTypeDto } from "./dto/request/update-fueltype.dto";


@Injectable()
export class FuelTypeService {
    constructor(
        private fuelTypeRepo: FuelTypeRepository
    ) { }
    async create(input: AddFuelTypeDto) {
        const { name } = input
        const existingName = await this.fuelTypeRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.fuelTypeRepo.insert(input)
    }
    async update(id, body: UpdateFuelTypeDto) {
        const { name } = body
        const existingFuelType = await this.fuelTypeRepo.findById(id)
        if (!existingFuelType) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.fuelTypeRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.fuelTypeRepo.update(id, body)
    }
    async get(query: QueryFuelTypeDto) {
        const option = await this.fuelTypeRepo.mapOptionsQuery(query)
        return await this.fuelTypeRepo.find(option)
    }
    async getById(id) {
        const fuelType = await this.fuelTypeRepo.findById(id)
        if (!fuelType) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return fuelType
    }
    async delete(id) {
        const fuelType = await this.fuelTypeRepo.findById(id)
        if (!fuelType) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.fuelTypeRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}