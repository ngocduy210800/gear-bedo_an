import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { FuelTypeService } from "./fueltype.service";
import { QueryFuelTypeDto } from "./dto/request/query-fueltype.dto";
import { UpdateFuelTypeDto } from "./dto/request/update-fueltype.dto";
import { AddFuelTypeDto } from "./dto/request/add-fueltype.dto";
import { ApiSecurity, ApiTags } from "@nestjs/swagger";
@Controller("/fueltype")
@ApiTags("Fuel type")
export class FuelTypeController {
    constructor(private fuelTypeService: FuelTypeService) { }
    @Post()
    create(@Body() body: AddFuelTypeDto) {
        return this.fuelTypeService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateFuelTypeDto) {
        return this.fuelTypeService.update(id, body)
    }
    @Get()
    get(@Query() queryFuelTypeDto: QueryFuelTypeDto) {
        return this.fuelTypeService.get(queryFuelTypeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.fuelTypeService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.fuelTypeService.delete(id)
    }
}