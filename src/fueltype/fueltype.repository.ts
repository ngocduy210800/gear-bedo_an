import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { FuelType, FuelTypeDocument } from "./fueltype.entity";
import { QueryFuelTypeDto } from "./dto/request/query-fueltype.dto";

@Injectable()
export class FuelTypeRepository extends AbstractMongodbRepository<FuelType> {
    constructor(
        @InjectModel(FuelType.name) private fuelTypeModel: Model<FuelTypeDocument>
    ) {
        super(fuelTypeModel)
    }
    async mapOptionsQuery(queryFuelTypeDto: QueryFuelTypeDto) {
        const { page, size, id, createdAt, orderBy =  { createdAt: -1 }, name, green } = queryFuelTypeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (green) { options.where = { ...options.where, green } }
        return options;
    }

}