import {  Module } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { FAQModule } from "src/faq/faq.module";
import { FAQCategoryController} from "./faqcategory.controller";
import { FAQCategory, FAQCategorySchema } from "./faqcategory.entity";
import { FAQCategoryRepository } from "./faqcategory.repository";
import { FAQCategoryService } from "./faqcategory.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: FAQCategory.name, schema: FAQCategorySchema }]),
        FAQModule
    ],
    controllers: [FAQCategoryController],
    providers: [FAQCategoryRepository, FAQCategoryService],
    exports: [FAQCategoryRepository]
})
export class FAQCategoryModule { }