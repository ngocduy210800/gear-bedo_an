import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddFAQCategoryDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    name: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

}
