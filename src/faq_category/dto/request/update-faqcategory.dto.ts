import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsOptional, IsString, } from "class-validator";

export class UpdateFAQCategoryDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string
}
