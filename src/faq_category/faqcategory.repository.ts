import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { FAQCategory, FAQCategoryDocument } from "./faqcategory.entity";
import { QueryFAQCategoryDto } from "./dto/request/query-faqcategory.dto";

@Injectable()
export class FAQCategoryRepository extends AbstractMongodbRepository<FAQCategory> {
    constructor(
        @InjectModel(FAQCategory.name) private FAQCategoryModel: Model<FAQCategoryDocument>
    ) {
        super(FAQCategoryModel)
    }
    async mapOptionsQuery(queryMakeDto: QueryFAQCategoryDto) {
        const { page, size, id, orderBy =  { createdAt: -1 }, name, createdAt } = queryMakeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (createdAt) { options.where = { ...options.where, createdAt } }
        return options;
    }

}