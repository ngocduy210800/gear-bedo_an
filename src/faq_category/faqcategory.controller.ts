import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryFAQCategoryDto } from "./dto/request/query-faqcategory.dto";
import { UpdateFAQCategoryDto } from "./dto/request/update-faqcategory.dto";
import { AddFAQCategoryDto } from "./dto/request/add-faqcategory.dto";
import { FAQCategoryService } from "./faqcategory.service";
import { ApiTags } from "@nestjs/swagger";
@Controller("/faqcategory")
@ApiTags("faqcategory")
export class FAQCategoryController {
    constructor(private _service: FAQCategoryService) { }
    @Post()
    create(@Body() body: AddFAQCategoryDto) {
        return this._service.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateFAQCategoryDto) {
        return this._service.update(id, body)
    }
    @Get()
    get(@Query() queryMakeDto: QueryFAQCategoryDto) {
        return this._service.get(queryMakeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this._service.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this._service.delete(id)
    }
}