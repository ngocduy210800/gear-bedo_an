import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FAQCategoryRepository } from './faqcategory.repository';
import { AddFAQCategoryDto } from './dto/request/add-faqcategory.dto';
import { QueryFAQCategoryDto } from './dto/request/query-faqcategory.dto';
import { UpdateFAQCategoryDto } from './dto/request/update-faqcategory.dto';
import { FAQRepository } from 'src/faq/faq.repository';
@Injectable()
export class FAQCategoryService {
  constructor(
    private faqCategoryService: FAQCategoryRepository,
    private faqRepo: FAQRepository,
  ) {}
  async create(input: AddFAQCategoryDto) {
    const { name } = input;
    const existingName = await this.faqCategoryService.findOne({ name });
    if (existingName)
      throw new HttpException('Name already exists', HttpStatus.BAD_REQUEST);
    return await this.faqCategoryService.insert(input);
  }
  async update(id, body: UpdateFAQCategoryDto) {
    const { name } = body;
    const existingFAQCategory = await this.faqCategoryService.findById(id);
    if (!existingFAQCategory)
      throw new HttpException('Not found', HttpStatus.BAD_REQUEST);
    if (name) {
      const existingName = await this.faqCategoryService.findOne({ name });
      if (existingName)
        throw new HttpException('Name already exists', HttpStatus.BAD_REQUEST);
    }
    return await this.faqCategoryService.update(id, body);
  }
  async get(query: QueryFAQCategoryDto) {
    const option = await this.faqCategoryService.mapOptionsQuery(query);
    let result = await this.faqCategoryService.find(option);
    result.docs=await Promise.all(
      result.docs.map(async item => {
        const faq = await this.faqRepo.find({ categoryId: item.id });
        Object.assign(item._doc, {
          faq: faq.docs,
        });
        return item
      }),
    );
    return result;
  }
  async getById(id) {
    const faqCategory = await this.faqCategoryService.findById(id);
    if (!faqCategory)
      throw new HttpException('NOT_FOUND', HttpStatus.BAD_REQUEST);
    return faqCategory;
  }
  async delete(id) {
    const faqCategory = await this.faqCategoryService.findById(id);
    if (!faqCategory)
      throw new HttpException('NOT_FOUND', HttpStatus.BAD_REQUEST);
    const rs = await this.faqCategoryService.delete(id);
    if (rs) return 'success';
    throw new HttpException('Fail', HttpStatus.BAD_REQUEST);
  }
}
