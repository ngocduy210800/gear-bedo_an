import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type FAQCategoryDocument = FAQCategory & Document;

@Schema({ timestamps: true })
export class FAQCategory {
    @Prop()
    name: string

    @Prop()
    description: string
}
export const FAQCategorySchema = SchemaFactory.createForClass(FAQCategory);