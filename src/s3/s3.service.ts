import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import * as AWS from "aws-sdk"
import mime from "mime";
import { ModelRepository } from "src/model/model.repository";

AWS.config.update({
    accessKeyId: process.env.ACCESS_KEY_S3,
    secretAccessKey: process.env.SECRET_KEY_S3,
    region: "ap-southeast-2"
})
@Injectable()
export class S3Service {
    private s3
    constructor(
    ) {
        this.s3 = new AWS.S3()
    }
    async uploadImage(file) {
        const key = `${new Date().getTime()}-${file.originalname.split(" ").join("")}`
        const result = await this.s3.putObject({
            Bucket: "gear-nz",
            Body: file.buffer,
            Key: key,
            ContentType: file.mimetype
        })
            .promise()
            .then(res => {
                console.log(`Upload succeeded - `, res);
                return `https://gear-nz.s3.ap-southeast-2.amazonaws.com/${key}`
            })
            .catch(err => {
                console.log("Upload failed:", err);
                throw new HttpException("err", HttpStatus.BAD_REQUEST)
            });
        return {
            url: result
        }
    }
}