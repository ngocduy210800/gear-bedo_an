import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query, UploadedFile, UseInterceptors } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { S3Service } from "./s3.service";
import { ApiTags } from "@nestjs/swagger";
import { AnyFilesInterceptor, FileInterceptor } from "@nestjs/platform-express";
import { Express } from 'express'

@Controller("/s3")
@ApiTags("s3")
export class S3Controller {
    constructor(private s3Service: S3Service) { }
    @Post('upload')
    @UseInterceptors(FileInterceptor('file'))
    create(@UploadedFile() file: Express.Multer.File) {
        return this.s3Service.uploadImage(file)
    }
}