import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { BodyTypeService } from "./bodytype.service";
import { AddBodyTypeDto } from "./dto/request/add-bodytype.dto";
import { QueryBodyTypeDto } from "./dto/request/query-bodytype.dto";
import { UpdateBodyTypeDto } from "./dto/request/update-bodytype.dto";


@Controller("/bodytype")
@ApiTags("Body Type")
export class BodyTypeController {
    constructor(private bodyTypeService: BodyTypeService) { }
    @Post()
    create(@Body() body: AddBodyTypeDto) {
        return this.bodyTypeService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateBodyTypeDto) {
        return this.bodyTypeService.update(id, body)
    }
    @Get()
    get(@Query() queryBodyTypeDto: QueryBodyTypeDto) {
        return this.bodyTypeService.get(queryBodyTypeDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.bodyTypeService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.bodyTypeService.delete(id)
    }
}