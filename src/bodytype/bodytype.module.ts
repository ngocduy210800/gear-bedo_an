import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { BodyTypeController } from "./bodytype.controller";
import { BodyType, BodyTypeSchema } from "./bodytype.entity";
import { BodyTypeRepository } from "./bodytype.repository";
import { BodyTypeService } from "./bodytype.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: BodyType.name, schema: BodyTypeSchema }])],
    controllers: [BodyTypeController],
    providers: [BodyTypeRepository, BodyTypeService],
    exports: [BodyTypeRepository]
})
export class BodyTypeModule { }