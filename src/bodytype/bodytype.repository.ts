import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { BodyType, BodyTypeDocument } from "./bodytype.entity";
import { QueryBodyTypeDto } from "./dto/request/query-bodytype.dto";

@Injectable()
export class BodyTypeRepository extends AbstractMongodbRepository<BodyType> {
    constructor(
        @InjectModel(BodyType.name) private bodyTypeModel: Model<BodyTypeDocument>
    ) {
        super(bodyTypeModel)
    }
    async mapOptionsQuery(queryBodyTypeDto: QueryBodyTypeDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, name } = queryBodyTypeDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        return options;
    }

}