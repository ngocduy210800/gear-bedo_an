import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { BodyTypeRepository } from "./bodytype.repository";
import { AddBodyTypeDto } from "./dto/request/add-bodytype.dto";
import { QueryBodyTypeDto } from "./dto/request/query-bodytype.dto";
import { UpdateBodyTypeDto } from "./dto/request/update-bodytype.dto";


@Injectable()
export class BodyTypeService {
    constructor(
        private bodyTypeRepo: BodyTypeRepository
    ) { }
    async create(input: AddBodyTypeDto) {
        const { name } = input
        const existingBodyType = await this.bodyTypeRepo.findOne({ name })
        if (existingBodyType) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.bodyTypeRepo.insert(input)
    }
    async update(id, body: UpdateBodyTypeDto) {
        const { name } = body
        const existingBodyType = await this.bodyTypeRepo.findById(id)
        if (!existingBodyType) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.bodyTypeRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.bodyTypeRepo.update(id, body)
    }
    async get(query: QueryBodyTypeDto) {
        const option = await this.bodyTypeRepo.mapOptionsQuery(query)
        return await this.bodyTypeRepo.find(option)
    }
    async getById(id) {
        const bodyType = await this.bodyTypeRepo.findById(id)
        if (!bodyType) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return bodyType
    }
    async delete(id) {
        const bodyType = await this.bodyTypeRepo.findById(id)
        if (!bodyType) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.bodyTypeRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}