import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type BodyTypeDocument = BodyType & Document;

@Schema({ timestamps: true })
export class BodyType {
    @Prop()
    name: string

    @Prop()
    description: string
    
}
export const BodyTypeSchema = SchemaFactory.createForClass(BodyType);