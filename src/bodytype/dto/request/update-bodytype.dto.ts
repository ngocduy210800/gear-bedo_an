import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class UpdateBodyTypeDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string
}
