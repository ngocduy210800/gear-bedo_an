import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { CarFeatureController } from "./career.controller";
import { Career, CareerSchema } from "./career.entity";
import { CareerRepository } from "./career.repository";
import { CareerService } from "./career.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: Career.name, schema: CareerSchema }])],
    controllers: [CarFeatureController],
    providers: [CareerRepository, CareerService],
    exports: [CareerRepository]
})
export class CareerModule { }