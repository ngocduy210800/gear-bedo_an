import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type CareerDocument = Career & Document;

@Schema({ timestamps: true })
export class Career {
    @Prop()
    name: string

    @Prop()
    description: string

    @Prop()
    note: string

    @Prop()
    location: string

    @Prop()
    requirements: string

    @Prop()
    expertise: string

    @Prop({ unique: true })
    slug: string

    @Prop()
    expiredDate: Date
}
export const CareerSchema = SchemaFactory.createForClass(Career);