import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class UpdateCareerDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    note: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    location: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    requirements: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    expertise: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    slug: string

    @IsOptional()
    @ApiProperty({ required: false })
    expiredDate: string
}
