import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddCareerDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    name: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    note: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    location: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    requirements: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    expertise: string

    @IsNotEmpty()
    @ApiProperty()
    expiredDate: Date

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    slug: string
}
