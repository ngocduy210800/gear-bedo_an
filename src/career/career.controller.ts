import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { CareerService } from "./career.service";
import { AddCareerDto } from "./dto/request/add-career.dto";
import { QueryCareerDto } from "./dto/request/query-career.dto";
import { UpdateCareerDto } from "./dto/request/update-career.dto";
@Controller("/career")
@ApiTags("Career")
export class CarFeatureController {
    constructor(private careerService: CareerService) { }
    @Post()
    create(@Body() body: AddCareerDto) {
        return this.careerService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateCareerDto) {
        return this.careerService.update(id, body)
    }
    @Get()
    get(@Query() queryCarFeatureDto: QueryCareerDto) {
        return this.careerService.get(queryCarFeatureDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.careerService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.careerService.delete(id)
    }
}