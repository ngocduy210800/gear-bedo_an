import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Career, CareerDocument } from "./career.entity";
import { QueryCareerDto } from "./dto/request/query-career.dto";

@Injectable()
export class CareerRepository extends AbstractMongodbRepository<Career> {
    constructor(
        @InjectModel(Career.name) private careerModel: Model<CareerDocument>
    ) {
        super(careerModel)
    }
    async mapOptionsQuery(queryCarFeatureDto: QueryCareerDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, name, location, expertise, requirements, expiredDate, slug } = queryCarFeatureDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (location) { options.where = { ...options.where, location } }
        if (expertise) { options.where = { ...options.where, expertise } }
        if (requirements) { options.where = { ...options.where, requirements } }
        if (expiredDate) { options.where = { ...options.where, expiredDate } }
        if (slug) { options.where = { ...options.where, slug } }
        return options;
    }

}