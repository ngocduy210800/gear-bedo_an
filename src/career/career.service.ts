import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CareerRepository } from "./career.repository";
import { AddCareerDto } from "./dto/request/add-career.dto";
import { QueryCareerDto } from "./dto/request/query-career.dto";
import { UpdateCareerDto } from "./dto/request/update-career.dto";
@Injectable()
export class CareerService {
    constructor(
        private careerRepo: CareerRepository
    ) { }
    async create(input: AddCareerDto) {
        const { slug } = input
        const existingSlug = await this.careerRepo.findOne({ slug })
        if (existingSlug) throw new HttpException("Slug already exists", HttpStatus.BAD_REQUEST)
        return await this.careerRepo.insert(input)
    }
    async update(id, body: UpdateCareerDto) {
        const { slug } = body
        const existingCareer = await this.careerRepo.findById(id)
        if (!existingCareer) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (slug) {
            const existingSlug = await this.careerRepo.findOne({ slug })
            if (existingSlug) throw new HttpException("Slug already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.careerRepo.update(id, body)
    }
    async get(query: QueryCareerDto) {
        const option = await this.careerRepo.mapOptionsQuery(query)
        return await this.careerRepo.find(option)
    }
    async getById(id) {
        const career = await this.careerRepo.findById(id)
        if (!career) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return career
    }
    async delete(id) {
        const career = await this.careerRepo.findById(id)
        if (!career) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.careerRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}