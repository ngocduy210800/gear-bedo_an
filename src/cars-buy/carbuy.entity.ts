import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { STATUS_CAR_BUY } from './constant/constant';
export type CarBuyDocument = CarBuy & Document;

@Schema({ timestamps: true })
export class CarBuy {
    @Prop()
    carSellId: string

    @Prop()
    priceCar: number

    @Prop()
    userBuyId: string

    @Prop({ type: "object" })
    purchaserInformation: object

    @Prop()
    taxRate: number

    @Prop()
    referenceNumber: string

    @Prop({ type: "object" })
    pickUpInfor: object

    @Prop({ type: "object" })
    deliveryInfor: object

    @Prop()
    convenientTime: Date

    // @Prop({ type: [String] })
    // buyMoreId: string[]

    @Prop()
    totalPrice: number

    @Prop({ type: "object" })
    driverLicenseInfor: object

    @Prop()
    createdBy: string

    @Prop()
    updateBy: string

    @Prop({ enum: STATUS_CAR_BUY })
    status: number

    @Prop({ type: "object" })
    paymentInfor: object
}
export const CarBuySchema = SchemaFactory.createForClass(CarBuy);