import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
export class PaymentDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    token: string
}
