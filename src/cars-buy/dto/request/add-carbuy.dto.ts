import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsDate, IsEnum, IsNotEmpty, IsNumber, IsObject, IsOptional, IsString, ValidateNested, } from "class-validator";
import moment from "moment";
export class PickUpInfor {
    @IsNotEmpty()
    @ApiProperty({ type: "object", required: true })
    garage: object

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty({ required: true, description: "số tiền khách phải đóng, thường là free" })
    price: number
}
export class DeliveryInfor {
    @IsOptional()
    @ApiProperty({ type: "object", required: true })
    garage: object

    @IsNotEmpty()
    @ApiProperty({ type: "object", required: true })
    address: object

    @IsOptional()
    @ApiProperty({ type: "number", required: false })
    @IsNumber()
    price: number
}

export class AddCarBuyDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    carSellId: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    userBuyId: string

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    purchaserInformation: object

    @IsOptional()
    @IsObject()
    @Type(() => PickUpInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: PickUpInfor, required: false })
    pickUpInfor?: PickUpInfor

    @IsOptional()
    @IsObject()
    @Type(() => DeliveryInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: DeliveryInfor, required: false })
    deliveryInfor?: DeliveryInfor

    @IsOptional()
    @Type(() => Date)
    @ApiProperty({ type: Date, required: false })
    convenientTime: Date

    // @IsOptional()
    // @ApiProperty({ required: false })
    // buyMoreId?: string[]

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    driverLicenseInfor: object

    @IsOptional()
    @ApiProperty({ required: false })
    createdBy: string

    @IsOptional()
    @ApiProperty({ required: false })
    updateBy: string

    @IsOptional()
    @ApiProperty({ required: false })
    referenceNumber: string
}
