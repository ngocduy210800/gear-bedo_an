import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsEnum, isJSON, IsNotEmpty, IsOptional, IsString, } from "class-validator";
import * as moment from 'moment'
import { STATUS_CAR_BUY } from "src/cars-buy/constant/constant";

export class QueryCarBuyDto {
    @IsOptional()
    @ApiProperty({ required: false })
    public id?: string;

    @IsOptional()
    @ApiProperty({ required: false, default: 1 })
    public page?: Number;

    @IsOptional()
    @ApiProperty({ required: false, default: 100 })
    public size?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : null;
    })
    @ApiProperty({ required: false, type: "object", description: "de sort", example: "orderBy:{ name: 1 }" })
    public orderBy?: object;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query tu ngay den ngay", example: "createdAt={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public createdAt?: object;

    @IsOptional()
    @ApiProperty({ required: false })
    public carSellId?: string;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query lte,gte", example: "priceCar={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public priceCar?: Number;

    @IsOptional()
    @ApiProperty({ required: false })
    public userBuyId?: string;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query lte,gte", example: "totalPrice={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public totalPrice?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query lte,gte", example: "taxRate={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public taxRate?: Number;

    @IsOptional()
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    @ApiProperty({ required: false, type: "any", description: "co the query lte,gte", example: "convenientTime={'$gte':'yyyy-mm-dd','$lte':'yyyy-mm-dd'}" })
    public convenientTime?: Number;

    @IsOptional()
    @ApiProperty({ required: false, type: "any", description: "-1: khach hang refund,0: khoi tao,1: da thanh toan,2: da ban giao xe" })
    @Transform(({ value }) => {
        return isJSON(value) ? JSON.parse(value) : value;
    })
    status?: number

    @IsOptional()
    @ApiProperty({ required: false })
    referenceNumber: string
}

