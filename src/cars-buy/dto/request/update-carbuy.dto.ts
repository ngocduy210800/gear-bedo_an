import { ApiProperty } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsDate, IsEnum, IsNotEmpty, IsNumber, IsObject, IsOptional, IsString, ValidateNested, } from "class-validator";
import moment from "moment";
import { STATUS_CAR_BUY } from "src/cars-buy/constant/constant";
export class PickUpInfor {
    @IsNotEmpty()
    garage: object

    @IsNotEmpty()
    @IsNumber()
    price: number
}
export class DeliveryInfor {
    @IsOptional()
    garage?: object

    @IsNotEmpty()
    address: object

    @IsOptional()
    @IsNumber()
    price?: number
}
export class UpdateCarBuyDto {

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    purchaserInformation?: object

    @IsOptional()
    @IsObject()
    @Type(() => PickUpInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: PickUpInfor, required: false })
    pickUpInfor?: PickUpInfor

    @IsOptional()
    @IsObject()
    @Type(() => DeliveryInfor)
    @ValidateNested({ each: true })
    @ApiProperty({ type: DeliveryInfor, required: false })
    deliveryInfor?: DeliveryInfor
    
    @IsOptional()
    @Type(() => Date)
    @ApiProperty({ type: "Date", required: false })
    convenientTime?: Date

    @IsOptional()
    @ApiProperty({ type: "object", required: false })
    driverLicenseInfor?: object

    @IsOptional()
    @ApiProperty({ required: false })
    createdBy?: string

    @IsOptional()
    @ApiProperty({ required: false })
    updateBy?: string

    @IsOptional()
    @IsEnum(STATUS_CAR_BUY)
    @ApiProperty({ required: false })
    status?: number

    @IsOptional()
    @ApiProperty({ required: false })
    referenceNumber: string
}
