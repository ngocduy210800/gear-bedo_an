import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { BuyMoreRepository } from "src/buymore/buymore.repository";
import { CarSellRepository } from "src/carsell/carsell.repository";
import { UserRepository } from "src/user/user.repository";
import { CarBuyRepository } from "./carbuy.repository";
import { AddCarBuyDto } from "./dto/request/add-carbuy.dto";
import { QueryCarBuyDto } from "./dto/request/query-carbuy.dto";
import { UpdateCarBuyDto } from "./dto/request/update-carbuy.dto";
import Stripe from 'stripe';
const stripe = new Stripe(process.env.PRIVATE_KEY_STRIPE, {
    apiVersion: '2022-08-01'
});
@Injectable()
export class CarBuyService {
    constructor(
        private carBuyRepo: CarBuyRepository,
        private carSellRepo: CarSellRepository,
        private userRepo: UserRepository,
        private buyMoreRepo: BuyMoreRepository
    ) { }
    async create(body: AddCarBuyDto) {
        const { carSellId, userBuyId, deliveryInfor, pickUpInfor } = body
        const [existingCarSell, existringUser] = await Promise.all([
            this.carSellRepo.findOne({ _id: carSellId, status: 4 }),
            this.userRepo.findById(userBuyId)
        ])
        if (!existingCarSell) throw new HttpException("NOT FOUND CAR", HttpStatus.BAD_REQUEST)
        if (!existringUser) throw new HttpException("NOT FOUND USER", HttpStatus.BAD_REQUEST)
        // let priceBuyMore = []
        // if (buyMoreId) {
        //     priceBuyMore = await Promise.all(
        //         buyMoreId.map(async item => {
        //             const buyMore = await this.buyMoreRepo.findById(item)
        //             return buyMore.price
        //         })
        //     )
        // }
        const priceCar = existingCarSell.priceBuy || 0
        const taxRate = existingCarSell?.taxRate || 0
        let totalPrice = priceCar + (priceCar * taxRate)
        // priceBuyMore.forEach(priceItem => {
        //     totalPrice += priceItem || 0
        // })
        if (deliveryInfor) {
            totalPrice += deliveryInfor?.price || 0
        }
        if (pickUpInfor) {
            totalPrice += pickUpInfor?.price || 0
        }
        await this.carSellRepo.update(carSellId, { status: 5 })
        const result = await this.carBuyRepo.insert({
            ...body,
            priceCar,
            taxRate,
            totalPrice,
            status: 0
        })
        return result
    }
    async update(id, body: UpdateCarBuyDto) {
        const { status } = body
        const carBuy = await this.carBuyRepo.findById(id)
        if (status && status == -1) {
            await this.carSellRepo.update(carBuy.carSellId, { status: 4 })
        }
        if (!carBuy) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return await this.carBuyRepo.update(id, body)
    }
    async get(query: QueryCarBuyDto) {
        const option = await this.carBuyRepo.mapOptionsQuery(query)
        const result = await this.carBuyRepo.find(option)
        await Promise.all(result.docs.map(async item => {
            const [user, carsell] = await Promise.all([
                this.userRepo.findById(item.userBuyId),
                this.carSellRepo.findById(item.carSellId),
            ])
            Object.assign(item._doc, {
                user,
                carsell,
            })
            return item
        }))
        return result
    }
    async getById(id) {
        const carBuy = await this.carBuyRepo.findById(id)
        if (!carBuy) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const [user, carsell] = await Promise.all([
            this.userRepo.findById(carBuy.userBuyId),
            this.carSellRepo.findById(carBuy.carSellId)
        ])
        Object.assign(carBuy._doc, {
            user,
            carsell
        })
        return carBuy
    }
    async delete(id) {
        const carBuy = await this.carBuyRepo.findById(id)
        if (!carBuy) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.carBuyRepo.update(id, { status: -1 })
        await this.carSellRepo.update(carBuy.carSellId, { status: 4 })
        return rs
    }
    async payment(id, body) {
        const { token } = body
        const carBuy = await this.carBuyRepo.findById(id)
        if (!carBuy) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        if (carBuy.status != 0) throw new HttpException("the carbuy is paid", HttpStatus.BAD_REQUEST)
        const payment = await stripe.charges.create({
            amount: carBuy.totalPrice,
            currency: process.env.CURRENCY,
            description: `paid for carbuy: ${carBuy._id}`,
            source: token
        })
        await this.carSellRepo.update(carBuy.carSellId, { status: 5 })
        return await this.carBuyRepo.update(id, { status: 1, paymentInfor: payment })
    }
    // async alo() {
    //     const payment = await stripe.tokens.create({
    //         card: {
    //             number: "4242 4242 4242 4242",
    //             exp_month: "2",
    //             exp_year: "2024",
    //             cvc: "212"
    //         }
    //     })
    //     return payment
    // }
}