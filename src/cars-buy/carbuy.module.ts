import {  MiddlewareConsumer, Module, NestModule, RequestMethod } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { AuthMiddleware } from "common/middleware/auth";
import { BuyMoreModule } from "src/buymore/buymore.module";
import { CarSellModule } from "src/carsell/carsell.module";
import { UserModule } from "src/user/user.module";
import { CarBuyController } from "./carbuy.controller";
import { CarBuy, CarBuySchema } from "./carbuy.entity";
import { CarBuyRepository } from "./carbuy.repository";
import { CarBuyService } from "./carbuy.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: CarBuy.name, schema: CarBuySchema }]),
        UserModule,
        CarSellModule,
        BuyMoreModule
    ],
    controllers: [CarBuyController],
    providers: [CarBuyRepository, CarBuyService],
    exports: [CarBuyRepository]
})
export class CarBuyModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            .exclude({ path: 'carbuy', method: RequestMethod.GET },
                { path: 'carbuy/:id', method: RequestMethod.GET })
            .forRoutes(CarBuyModule)
    }
}