import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ApiHeader, ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { CarBuyService } from "./carbuy.service";
import { AddCarBuyDto } from "./dto/request/add-carbuy.dto";
import { UpdateCarBuyDto } from "./dto/request/update-carbuy.dto";
import { QueryCarBuyDto } from "./dto/request/query-carbuy.dto";
import { PaymentDto } from "./dto/request/payment.dto";
@Controller("/carbuy")
@ApiTags("Car Buy")
export class CarBuyController {
    constructor(private carBuyService: CarBuyService) { }
    @Post()
    create(@Body() body: AddCarBuyDto) {
        return this.carBuyService.create(body)
    }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Post("/:id/payment")
    payment(@Param("id") id: string, @Body() body: PaymentDto) {
        return this.carBuyService.payment(id, body)
    }
    @Get("/:id")
    getByid(@Param("id") id: string) {
        return this.carBuyService.getById(id)
    }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.carBuyService.delete(id)
    }
    @Get()
    get(@Query() query: QueryCarBuyDto) {
        return this.carBuyService.get(query)
    }
    @ApiHeader({
        name: 'Authorization',
        description:'truyen token firebase len',
    })
    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateCarBuyDto) {
        return this.carBuyService.update(id, body)
    }
    // @Get("/alo/alo")
    // alo(){
    //     return this.carBuyService.alo()
    // }
}