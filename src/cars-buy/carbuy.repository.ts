import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { CarBuy, CarBuyDocument } from "./carbuy.entity";
import { QueryCarBuyDto } from "./dto/request/query-carbuy.dto";
import { InjectModel } from "@nestjs/mongoose";

@Injectable()
export class CarBuyRepository extends AbstractMongodbRepository<CarBuy> {
    constructor(
        @InjectModel(CarBuy.name) private carBuyModel: Model<CarBuyDocument>
    ) {
        super(carBuyModel)
    }
    async mapOptionsQuery(queryCarBuyDto: QueryCarBuyDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, status, carSellId, convenientTime, priceCar, taxRate, totalPrice, userBuyId, referenceNumber } = queryCarBuyDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (status) { options.where = { ...options.where, status } }
        if (carSellId) { options.where = { ...options.where, carSellId } }
        if (convenientTime) { options.where = { ...options.where, convenientTime } }
        if (priceCar) { options.where = { ...options.where, priceCar } }
        if (taxRate) { options.where = { ...options.where, taxRate } }
        if (totalPrice) { options.where = { ...options.where, totalPrice } }
        if (userBuyId) { options.where = { ...options.where, userBuyId } }
        if (referenceNumber) { options.where = { ...options.where, referenceNumber } }
        return options;
    }

}