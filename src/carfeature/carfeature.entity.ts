import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type CarFeatureDocument = CarFeature & Document;

@Schema({ timestamps: true })
export class CarFeature {
    @Prop()
    name: string

    @Prop()
    description: string
}
export const CarFeatureSchema = SchemaFactory.createForClass(CarFeature);