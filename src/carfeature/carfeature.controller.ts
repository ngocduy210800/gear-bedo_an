import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { Headers } from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { CarFeatureService } from "./carfeature.service";
import { AddCarFeatureDto } from "./dto/request/add-carfeature.dto";
import { QueryCarFeatureDto } from "./dto/request/query-carfeature.dto";
import { UpdateCarFeatureDto } from "./dto/request/update-carfeature.dto";
@Controller("/carfeature")
@ApiTags("Car Feature")
export class CarFeatureController {
    constructor(private carFeatureService: CarFeatureService) { }
    @Post()
    create(@Body() body: AddCarFeatureDto) {
        return this.carFeatureService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateCarFeatureDto) {
        return this.carFeatureService.update(id, body)
    }
    @Get()
    get(@Query() queryCarFeatureDto: QueryCarFeatureDto) {
        return this.carFeatureService.get(queryCarFeatureDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.carFeatureService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.carFeatureService.delete(id)
    }
}