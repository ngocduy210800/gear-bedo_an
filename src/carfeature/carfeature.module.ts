import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { CarFeatureController } from "./carfeature.controller";
import { CarFeature, CarFeatureSchema } from "./carfeature.entity";
import { CarFeatureRepository } from "./carfeature.repository";
import { CarFeatureService } from "./carfeature.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: CarFeature.name, schema: CarFeatureSchema }])],
    controllers: [CarFeatureController],
    providers: [CarFeatureRepository, CarFeatureService],
    exports: [CarFeatureRepository]
})
export class CarFeatureModule { }