import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CarFeatureRepository } from "./carfeature.repository";
import { AddCarFeatureDto } from "./dto/request/add-carfeature.dto";
import { QueryCarFeatureDto } from "./dto/request/query-carfeature.dto";
import { UpdateCarFeatureDto } from "./dto/request/update-carfeature.dto";
@Injectable()
export class CarFeatureService {
    constructor(
        private carFeatureRepo: CarFeatureRepository
    ) { }
    async create(input: AddCarFeatureDto) {
        const { name } = input
        const existingName = await this.carFeatureRepo.findOne({ name })
        if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        return await this.carFeatureRepo.insert(input)
    }
    async update(id, body: UpdateCarFeatureDto) {
        const { name } = body
        const existingCarFeature = await this.carFeatureRepo.findById(id)
        if (!existingCarFeature) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.carFeatureRepo.findOne({ name })
            if (existingName) throw new HttpException("Name already exists", HttpStatus.BAD_REQUEST)
        }
        return await this.carFeatureRepo.update(id, body)
    }
    async get(query: QueryCarFeatureDto) {
        const option = await this.carFeatureRepo.mapOptionsQuery(query)
        return await this.carFeatureRepo.find(option)
    }
    async getById(id) {
        const carFeature = await this.carFeatureRepo.findById(id)
        if (!carFeature) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        return carFeature
    }
    async delete(id) {
        const carFeature = await this.carFeatureRepo.findById(id)
        if (!carFeature) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.carFeatureRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}