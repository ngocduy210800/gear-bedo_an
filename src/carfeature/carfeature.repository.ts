import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { CarFeature, CarFeatureDocument } from "./carfeature.entity";
import { QueryCarFeatureDto } from "./dto/request/query-carfeature.dto";

@Injectable()
export class CarFeatureRepository extends AbstractMongodbRepository<CarFeature> {
    constructor(
        @InjectModel(CarFeature.name) private carFeatureModel: Model<CarFeatureDocument>
    ) {
        super(carFeatureModel)
    }
    async mapOptionsQuery(queryCarFeatureDto: QueryCarFeatureDto) {
        const { page, size, id, createdAt, orderBy = { createdAt: -1 }, name } = queryCarFeatureDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (createdAt) { options.where = { ...options.where, createdAt } }
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        return options;
    }

}