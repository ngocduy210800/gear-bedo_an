import { Module } from "@nestjs/common";

import { MongooseModule } from '@nestjs/mongoose';
import { MakeModule } from "src/make/make.module";
import { ModelController } from "./model.controller";
import { Model, ModelSchema } from "./model.entity";
import { ModelRepository } from "./model.repository";
import { ModelService } from "./model.service";



@Module({
    imports: [
        MongooseModule.forFeature([{ name: Model.name, schema: ModelSchema }]),
        MakeModule
    ],
    controllers: [ModelController],
    providers: [ModelRepository, ModelService],
    exports: [ModelRepository]
})
export class ModelModule { }