import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type ModelDocument = Model & Document;

@Schema({ timestamps: true })
export class Model {
    @Prop()
    name: string

    @Prop()
    description: string

    @Prop()
    makeId: string
}
export const ModelSchema = SchemaFactory.createForClass(Model);