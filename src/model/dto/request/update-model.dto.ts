import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsOptional, IsString, } from "class-validator";

export class UpdateModelDto {
    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    name?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    makeId?: string
}
