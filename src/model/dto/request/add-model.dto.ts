import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, } from "class-validator";

export class AddModelDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    name: string

    @IsOptional()
    @IsString()
    @ApiProperty({ required: false })
    description?: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty({ required: true })
    makeId: string
}
