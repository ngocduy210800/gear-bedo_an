import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AbstractMongodbRepository } from "common/repository/mongo.repository";
import { Model as ModelEntity, ModelDocument } from "./model.entity";
import { QueryModelDto } from "./dto/request/query-model.dto";

@Injectable()
export class ModelRepository extends AbstractMongodbRepository<ModelEntity> {
    constructor(
        @InjectModel(ModelEntity.name) private modelModel: Model<ModelDocument>
    ) {
        super(modelModel)
    }
    async mapOptionsQuery(queryModelDto: QueryModelDto) {
        const { page, size, id, orderBy =  { createdAt: -1 }, name, createdAt, makeId } = queryModelDto;
        let options = { pagination: { page: page, size: size }, where: {}, orderBy: orderBy };
        if (id) { options.where = { ...options.where, _id: id } }
        if (name) { options.where = { ...options.where, name: { $regex: name, $options: 'i' } } }
        if (makeId) { options.where = { ...options.where, makeId } }
        if (createdAt) { options.where = { ...options.where, createdAt } }

        return options;
    }

}