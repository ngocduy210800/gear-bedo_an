import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Query } from "@nestjs/common";
import { ReponseDto } from "common/decorators/reponse-dto.decorator";
import { QueryModelDto } from "./dto/request/query-model.dto";
import { UpdateModelDto } from "./dto/request/update-model.dto";
import { AddModelDto } from "./dto/request/add-model.dto";
import { ModelService } from "./model.service";
import { ApiTags } from "@nestjs/swagger";
@Controller("/model")
@ApiTags("Model")
export class ModelController {
    constructor(private modelService: ModelService) { }
    @Post()
    create(@Body() body: AddModelDto) {
        return this.modelService.create(body)
    }

    @Put("/:id")
    update(@Param("id") id: string, @Body() body: UpdateModelDto) {
        return this.modelService.update(id, body)
    }
    @Get()
    get(@Query() queryModelDto: QueryModelDto) {
        return this.modelService.get(queryModelDto)
    }
    @Get("/:id")
    getById(@Param("id") id: string) {
        return this.modelService.getById(id)
    }
    @Delete("/:id")
    delete(@Param("id") id: string) {
        return this.modelService.delete(id)
    }
}