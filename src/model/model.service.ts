import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { ModelRepository } from "./model.repository";
import { AddModelDto } from "./dto/request/add-model.dto";
import { QueryModelDto } from "./dto/request/query-model.dto";
import { UpdateModelDto } from "./dto/request/update-model.dto";
import { MakeRepository } from "src/make/make.repository";


@Injectable()
export class ModelService {
    constructor(
        private modelRepo: ModelRepository,
        private makeRepo: MakeRepository
    ) { }
    async create(input: AddModelDto) {
        const { name, makeId } = input
        const [existingName, existingMake] = await Promise.all([
            this.modelRepo.findOne({ name, makeId }),
            this.makeRepo.findById(makeId)
        ])
        if (existingName) throw new HttpException("Name with make already exists", HttpStatus.BAD_REQUEST)
        if (!existingMake) throw new HttpException("Not found makeId", HttpStatus.BAD_REQUEST)
        return await this.modelRepo.insert(input)
    }
    async update(id, body: UpdateModelDto) {
        const { name, makeId } = body
        const existingModel = await this.modelRepo.findById(id)
        if (!existingModel) throw new HttpException("Not found", HttpStatus.BAD_REQUEST)
        if (name) {
            const existingName = await this.modelRepo.findOne({ name, makeId: makeId || existingModel.makeId })
            if (existingName) throw new HttpException("Name with make already exists", HttpStatus.BAD_REQUEST)
        }
        if (makeId) {
            const existingMake = await this.makeRepo.findById(makeId)
            if (!existingMake) throw new HttpException("Not found makeId", HttpStatus.BAD_REQUEST)
        }
        return await this.modelRepo.update(id, body)
    }
    async get(query: QueryModelDto) {
        const option = await this.modelRepo.mapOptionsQuery(query)
        const result=await this.modelRepo.find(option)
        await Promise.all(result.docs.map(async item => {
            const make = await this.makeRepo.findById(item.makeId)
            Object.assign(item._doc, {
                make
            })
            return item
        }))
        return result
    }
    async getById(id) {
        const model = await this.modelRepo.findById(id)
        if (!model) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const make = await this.makeRepo.findById(model.makeId)
        Object.assign(model._doc, {
            make
        })
        return model
    }
    async delete(id) {
        const model = await this.modelRepo.findById(id)
        if (!model) throw new HttpException("NOT_FOUND", HttpStatus.BAD_REQUEST)
        const rs = await this.modelRepo.delete(id)
        if (rs) return "success"
        throw new HttpException("Fail", HttpStatus.BAD_REQUEST)
    }
}